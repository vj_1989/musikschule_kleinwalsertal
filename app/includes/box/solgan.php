<?php
global $staticlinks;
?>

<div class="solgan-container">
<span class="solgan-pinsel"  data-aos="fade-down-right"></span>
		<div class="solgan-wrap">
        	<div class="solgan-text caveat-font">
            „music is enough for a lifetime, but a lifetime is not enough for music.“
            </div>
            <div class="solgan-autor">
            Sergei Rachmaninoff
            </div>
              <div class="solgan-icon sprite">
            </div>
              <div class="solgan-btn">
              <a href="<?php echo $staticlinks['register']['url']; ?>#c" class="button btn-arrow sprite-after">deine Rede? auf was wartest du noch. </a>
            </div>
        </div>

</div>
