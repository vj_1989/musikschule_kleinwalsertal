<?php
global $contactform_sent, $staticlinks;

// Initial form class
$FORM = new Core_Form();

define('CF_TIME', time());
$CF_SETTINGS = array(
  'client'  => KUNDEN_BETRIEB, // Company name
  'email'   => KUNDEN_EMAIL, // Comany email
  'website' => str_replace(array('http:', '/', 'https:'), '', DOMAIN) // Company website
);

if ( strpos($_SERVER['SERVER_NAME'],'-dev1') ) {
  $CF_SETTINGS['email'] = 'lk@werbewind.com';
}

$CF_SETTINGS['subject']         = 'Ihre Anfrage an '.$CF_SETTINGS['website']; // Subject for customer
$CF_SETTINGS['subject_client']  = 'Anfrage auf '.$CF_SETTINGS['website']; // Subject for company
$CF_SETTINGS['client_response'] = 'Folgende Anfrage erreicht Sie über '.$CF_SETTINGS['website']; // Mail text for company

// Use captcha
// @todo deprecated
// @todo check for removal
$CF_SETTINGS['captcha']     = false;
$CF_SETTINGS['captcha_msg'] = 'Bitte füllen Sie den Spamschutz aus (die Zeichen im Bild unten links).';

// Add vCard as mail attachment
$CF_SETTINGS['vcard'] = true;

// Include technical data provided by Browscap class
// @todo deprecated
// @todo check for removal
$CF_SETTINGS['browscap'] = false;

// Allow saving for newsletter system
$CF_SETTINGS['newsletter'] = true;

// Optionally add advertising in mail to customer
$CF_SETTINGS['advert'] = '<tr><td colspan="2"></td></tr>';

// Response mail to customer - if this is an emtpy string no response will be sent
$CF_SETTINGS['response'] = '<br><br>vielen Dank für Ihre Nachricht. Wir werden uns schnellstmöglich mit Ihnen in Verbindung setzen.<br><br>Folgende Daten haben Sie uns übermittelt:<br><br>';

// Overwrite CSS for mail response
$CF_SETTINGS['mail_css'] = '';

$CF_SETTINGS['salutation']  = array(
  1 => array('feld' => LANG_CONTACT_SALUTATION_MS, 'text' => 'Sehr geehrte Frau'),
  2 => array('feld' => LANG_CONTACT_SALUTATION_MR, 'text' => 'Sehr geehrter Herr'),
  3 => array('feld' => LANG_CONTACT_SALUTATION_F,  'text' => 'Sehr geehrte Familie'),
  #4 => array('feld' => LANG_CONTACT_SALUTATION_C,   'text' => 'Sehr geehrte Damen und Herren'),
);
$CF_SETTINGS['pflichtfeld'] = '<strong title="'.LANG_CONTACT_MANDATORY.'">*</strong>';

$CF_FIELDS = array(
  'anrede' => array(
    'feld'    => LANG_CONTACT_SALUTATION,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_SALUTATION,
    'value'   => 1
  ),
  'vorname' => array(
    'feld'    => LANG_CONTACT_FIRSTNAME,
    'pflicht' => true,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_FIRSTNAME,
    'value'   => ''
  ),
  'nachname' => array(
    'feld'    => LANG_CONTACT_LASTNAME,
    'pflicht' => true,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_LASTNAME,
    'value'   => ''
  ),
  'firma' => array(
    'feld'    => LANG_CONTACT_COMPANY,
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'strasse' => array(
    'feld'    => LANG_CONTACT_STREET,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_STREET,
    'value'   => ''
  ),
  'hausnummer' => array(
    'feld'    => LANG_CONTACT_HOUSENR,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_HOUSENR,
    'value'   => ''
  ),
  'plz' => array(
    'feld'    => LANG_CONTACT_POSTCODE,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_POSTCODE,
    'value'   => ''
  ),
  'ort' => array(
    'feld'    => LANG_CONTACT_CITY,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_CITY,
    'value'   => ''
  ),
  'staat' => array(
    'feld'    => LANG_CONTACT_COUNTRY,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_COUNTRY,
    'value'   => ''
  ),
  'telefon' => array(
    'feld'    => LANG_CONTACT_PHONE,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_PHONE,
    'value'   => ''
  ),
  'mobil' => array(
    'feld'    => LANG_CONTACT_MOBILE,
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_MOBILE,
    'value'   => ''
  ),

  /*'fax' => array(
    'feld'    => 'Fax',
    'pflicht' => false,
    'msg'     => LANG_CONTACT_MANDATORY.' '.
  ),*/
  'email' => array(
    'feld'    => LANG_CONTACT_EMAIL,
    'pflicht' => true,
    'msg'     => LANG_CONTACT_MANDATORY.' '.LANG_CONTACT_EMAIL,
    'value'   => ''
  ),
  'anreise' => array(
    'feld'    => LANG_CONTACT_ARRIVAL,
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'abreise' => array(
    'feld'    => LANG_CONTACT_DEPARTURE,
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'personen' => array(
    'feld'    => LANG_CONTACT_PERSONS,
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'erwachsene' => array(
    'feld'    => LANG_CONTACT_ADULTS,
    'pflicht' => false,
    'msg'     => '',
   'value'   => '2'
  ),
  'alter_kind1' => array(
    'feld'    => LANG_CONTACT_CHILDRENAGE.' 1',
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'alter_kind2' => array(
    'feld'    => LANG_CONTACT_CHILDRENAGE.' 2',
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'alter_kind3' => array(
    'feld'    => LANG_CONTACT_CHILDRENAGE.' 3',
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'alter_kind4' => array(
    'feld'    => LANG_CONTACT_CHILDRENAGE.' 4',
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'nachricht' => array(
    'feld'    => LANG_CONTACT_MESSAGE,
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),
  'newsletter' => array(
    'feld'     => LANG_CONTACT_NEWSLETTER,
    'pflicht'  => false,
    'msg'      => '',
    'in_email' => false,
    'value'    => ''
  )
  /*'reservierung' => array(
    'feld'    => 'Reservierung',
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  ),*/
  /*'newsletter' => array(
    'feld'    => 'Newsletter',
    'pflicht' => false,
    'msg'     => '',
    'value'   => ''
  )*/
);

// Get posted values
foreach($CF_FIELDS as $f => $a){
  if(isset($_POST[$f])){
    $CF_FIELDS[$f]['value'] = $_POST[$f];
  }
  else if($f == 'telefon' || $f == 'mobil' || $f == 'fax'){
    if(isset($_POST[$f.'_0'])){
        $CF_FIELDS[$f]['value'] = array($_POST[$f.'_0'],$_POST[$f.'_1'],$_POST[$f.'_2']);
    }
  }
}

// Posted from other sites
// @todo check for removal of some of the items
if (isset($_POST['sa_anreise'])) {
    $CF_FIELDS['anreise']['value'] = $_POST['sa_anreise'];
}
if (isset($_POST['sa_naechte']) && !empty($CF_FIELDS['anreise']['value'])) {
    $temp = explode('-', $CF_FIELDS['anreise']['value']);
	if (count($temp) == 3) {
		$CF_FIELDS['abreise']['value'] = strftime("%Y-%m-%d", strtotime("+ ".(int)$_POST['sa_naechte']." days", mktime(0, 0, 0, $temp[1], $temp[2], $temp[0])));
	}
    else {
		$temp = explode('.', $CF_FIELDS['anreise']['value']);
		$CF_FIELDS['abreise']['value'] = strftime("%d.%m.%Y", strtotime("+ ".(int)$_POST['sa_naechte']." days", mktime(0, 0, 0, $temp[1], $temp[0], $temp[2])));
	}
}
else if (isset($_POST['sa_abreise'])) {
    $CF_FIELDS['abreise']['value'] = $_POST['sa_abreise'];
}
if (isset($_POST['sa_personen'])) {
    $CF_FIELDS['personen']['value'] = $_POST['sa_personen'];
}
if (isset($_POST['sa_erwachsene'])) {
    $CF_FIELDS['erwachsene']['value'] = $_POST['sa_erwachsene'];
}
if (isset($_POST['sa_kinder'])) {
    $CF_FIELDS['kinder']['value'] = $_POST['sa_kinder'];
}
if (isset($_GET['nachricht'])) {
    $CF_FIELDS['nachricht']['value'] = trim(htmlspecialchars(strip_tags($_GET['nachricht'])));
}

if (isset($_POST['kontaktform_send'])) {

  $CF = new ContactForm_Main();
  $CF->init($CF_FIELDS, $CF_SETTINGS);
  $CF->validate();

  if($CF->isValid()){

    $CF->sendMail();

    #$CF->setSource($_SESSION['VisitorSource']);

    $CF->saveToContactFormBackups($source);

    $aid = $CF->exportToAddressBook();

    // Optionally add address to a specific group in address book
    #$AB = new AddressBook();
    #$AB->addressToGroup($aid,/*$group_id*/);

    $contactform_sent = true;

  }

}
?>