<div class="routeplanner">
  <h2><?php echo LANG_ROUTE_HEADING; ?></h2>
  <form target="_blank" method="post" action="https://tools.werbewind.com/Routenplaner/routenplaner-redirect.php">
    <input type="hidden" name="ziel" value="<?php echo KUNDEN_STRASSE.', '.KUNDEN_ORT.', '.KUNDEN_LAND; ?>">
    <fieldset>
      <legend><?php echo LANG_ROUTE_LEGEND; ?></legend>
      <div class="cols cols-form">
				<div class="col-1">
          <label for="routenplaner-strasse"><?php echo LANG_CONTACT_STREET; ?></label>
          <input name="strasse" id="routenplaner-strasse" type="text" value="">
        </div>
        <div class="col-2">
          <label for="routenplaner-plz"><?php echo LANG_CONTACT_POSTCODE; ?></label>
          <input name="plz" id="routenplaner-plz" type="text" value="">
        </div>
      </div>
      <div class="cols cols-form">
				<div class="col-1">
          <label for="routenplaner-ort"><?php echo LANG_CONTACT_CITY; ?></label>
          <input name="ort" id="routenplaner-ort" type="text" value="">
        </div>
        <div class="col-2">
          <label for="routenplaner-land"><?php echo LANG_CONTACT_COUNTRY; ?></label>
          <select name="land" id="routenplaner-land">
          <?php
          $cs = array_slice(Lib_Locale::getCountries(), 1);
          foreach($cs as $co) {
            echo '<option value="'.$co.'">'.$co.'</option>';    
          }
          ?>
          </select>
        </div>
      </div>
      <div class="buttons">
        <button type="submit"><?php echo LANG_ROUTE_SUBMIT; ?></button>
      </div>
    </fieldset>
  </form>
</div>