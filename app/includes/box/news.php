<?php
$WW_NEWSBOX = new Newsbox_Main();

$boxes = $WW_NEWSBOX->getNewsboxes();

$activeboxes = array();
if (!empty($boxes)) {
  foreach($boxes as $box) {
    if ($box['status'] == 1) {
      $activeboxes[] = $box;
    }
  }
}

if (!empty($activeboxes)) {

  $content = '';
	
  $content .= '<div class="newsboxes-wrapper"><div class="newsboxes">';

  foreach($activeboxes as $box) {
		$box['bild'] = str_replace('https://kunden.werbewind.com/tools/newsbox/images/', IMG_PATH, $box['bild']);
    $content .= '<aside class="newsbox" role="complementary">';

    if (!empty($box['bild'])) {

      if (!empty($box['link'])) {
        $content .= '<a class="newsbox-pic" data-id="'.$box['boxnr'].'" href="'.$box['link'].'" target="'.$box['linkart'].'">';
      }

      $content .= '<img src="'.$box['bild'].'" alt="'.htmlspecialchars($box['titel']).'">';

      if (!empty($box['link'])) {
        $content .= '</a>';
      }

    }

    $content .= '<div class="newsbox-content">';

    $content .= '<div class="newsbox-heading caveat-font">'.$box['titel'].'</div>';
    $content .= '<div class="newsbox-text">'.$box['text'].'</div>';

    if (!empty($box['link'])) {

      if (empty($box['readmore_text'])) {
          $box['readmore_text'] = 'mehr dazu';
      }
      $content .= '<div class="newsbox-more"><a class="button btn-arrow sprite-after" data-id="'.$box['boxnr'].'" href="'.$box['link'].'" target="'.$box['linkart'].'">'.$box['readmore_text'].'</a></div>';

    }

    $content .= '</div>';

    $content .= '</aside>';

  }

  $content .= '</div></div>';

  echo $content;

}
?>