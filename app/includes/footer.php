

<div class="footer-wrapper">
  <footer class="footer">
    <div class="footer-box footer-box-1">

      <div class="footer-box-content">
      <div class="footer-box-content-1 contents">
        <div class="footer-logo-wrap"><a class="footer-logo" href=""></a></div>
        </div>
      	 <div class="footer-box-content-2 contents">
        <div class="footer-box-text1">Schulleitung</div>
        <div class="footer-box-text1">Direktor Michael Herrles</div>
     <div class="footer-box-text1">T  +43 664 8537910</div>
        </div>
        <div class="footer-box-content-3 contents">
         <div class="footer-box-text1">F  +43 5517 5315219</div>
        <div class="footer-box-text1">Engelbert-Kessler-Str. 34</div>
     <div class="footer-box-text1">A-6991 Riezlern </div>
        </div>
         <div class="footer-box-content-4 contents">

        <div class="footer-box-contact"><a class="button btn-arrow sprite-after" href="<?php echo $staticlinks['kontakt']['url']; ?>#c">Schreib uns.</a></div>
        </div>
      </div>
    </div>
    <div class="footer-box footer-box-2">

      <div class="footer-box-content">
        <ul class="footer-box-list">
          <li class="footer-box-list-item"><a href="<?php echo $staticlinks['downloads']['url']; ?>#c">   Downloads</a></li>
          <li class="footer-box-list-item"><a href="<?php echo $staticlinks['links']['url']; ?>#c">Links & Partner</a></li>
          <li class="footer-box-list-item"><a href="<?php echo $staticlinks['impressum']['url']; ?>#c">Impressum</a></li>
          <li class="footer-box-list-item"><a href="<?php echo $staticlinks['impressum']['url']; ?>#datenschutz">Datenschutz</a></li>
          <li class="footer-box-list-item"><a href="<?php echo $staticlinks['impressum']['url']; ?>#rechtliche-hinweise">rechtliche Hinweise</a></li>
        </ul>
      </div>
    </div>

  </footer>
  <div class="extra-footericon-wrap">
    <a href="http://www.musikschulwerk-vorarlberg.at/Startseite.html" target="_blank" class="sprite musikschulwerk-icon"></a>
   <a href="https://www.kleinwalsertal.com/de" target="_blank" class="sprite kleinwalsertal-icon"></a>
   </div>
</div>
