<?php
// Normally there is no need to change this variable
$include_dir = '../../../';

// Include init file for client configuration, autoloaders etc.
include 'includes/init.php';
?>
<!doctype html>
<html class="no-js" lang="<?php echo LANG; ?>">
  <head>
    <meta charset="utf-8">
    <title><?php echo $redcmsitem['seo_title']; ?></title>
    <meta name="description" content="<?php echo $redcmsitem['seo_description']; ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="canonical" href="<?php echo DOMAIN.$redcmsitem['url']; ?>">
    <?php
    // Custom meta tags
    echo $redcmssettings['metatags'].$redcmsitem['metatags'];
    ?>
    <link rel="shortcut icon" href="/favicon.ico">
    <!-- build:css(.tmp) /styles/main.css -->
    <link rel="stylesheet" href="../../bower_components/normalize-css/normalize.css">
    
    <link rel="stylesheet" href="../../bower_components/flexslider/flexslider.css">
    
    <link rel="stylesheet" href="../../bower_components/lightgallery/dist/css/lightgallery.css">
    <link rel="stylesheet" href="/styles/tools/lightGalleryWw.css">
    
    <link rel="stylesheet" href="../../bower_components/pickadate/lib/themes/default.css">
    <link rel="stylesheet" href="../../bower_components/pickadate/lib/themes/default.date.css">
    
     <link href="../../bower_components/aos/dist/aos.css" rel="stylesheet">
    
    <link rel="stylesheet" href="/styles/generic.css">
    <link rel="stylesheet" href="/styles/forms.css">
    <link rel="stylesheet" href="/styles/layout.css">
    <link rel="stylesheet" href="/styles/navi.css">
    <link rel="stylesheet" href="/styles/content.css">
    <link rel="stylesheet" href="/styles/tools.css">
    
    <link rel="stylesheet" href="/styles/tools/newsbox.css">
    
    <link rel="stylesheet" href="/styles/tools/events.css">
     <link rel="stylesheet" href="/styles/tools/rednews2.css">
    <link rel="stylesheet" href="/styles/tools/routeplanner.css">
      <link rel="stylesheet" href="/styles/tools/parallex.css">
     <link rel="stylesheet" href="/styles/tools/solgan.css">
    <link rel="stylesheet" href="/styles/print.css">
    <!-- endbuild -->
    
    
    <!-- build:js /scripts/modernizr.js -->
    <script src="/scripts/vendor/modernizr.js"></script>
    <!-- endbuild -->
    
  </head>
  <body class="<?php echo implode(' ', $bodyClass); ?>">
   <div class="body-wrapper">
    <a class="skiplink" href="#c">direkt zur Navigation</a>
    <a class="skiplink" href="#c">direkt zum Inhalt</a>
   
    <div class="header-wrapper">
      <header class="header" role="banner">
      <?php
      include 'includes/navi.php';
      ?>
      </header>
    </div>
    
    
    <?php
    include 'includes/flexslider.php';
    ?>
    
    
    <div class="clearfix content-wrapper">
		  <div id="c" class="anchor"></div>
		  <main id="content" role="main" class="clearfix content">
        <?php
       # echo '<div class="heading">';
        #echo '<h1>'.$redcmspage['titel'].'</h1>';
        if (!empty($redcmspage['untertitel'])) {
        #  echo '<h2 class="heading-sub">'.$redcmspage['untertitel'].'</h2>';
        }
        if (!empty($redcmspage['teaser'])) {
         # echo '<p class="heading-teaser">'.nl2br($redcmspage['teaser']).'</p>';
        }
        #echo '</div>';
        $REDCMS_TPL->printContents($redcmspage['inhalte']);
        ?>
      </main>
      <?php
			// Sidebar (if needed)
      /*if ($is_front !== true) {
      ?>
      <div class="sidebar">
        <?php
        include 'includes/box/navi.php';
        include 'includes/box/news2.php';
        ?>
      </div>
      <?php
      }*/
      ?>
    </div>

    
    <?php
	   if ($is_front == true){
		  echo '<a href="'.$staticlinks['bildergallery']['url'].'#c" class="sprite-after bildgallery-btn">mehr pics <span class="mob-galleryicon sprite"></span></a>';
    include 'includes/box/news.php';
	   }
    ?>
    
    <?php
			// Sidebar (if needed)
      if ($is_front == true){
		   include 'includes/box/solgan.php';
		  
	  }?>
    

    <?php
		include 'includes/footer.php';
		?>

    <div class="copyright-wrapper">
      <div class="clearfix copyright">
  
        <div class="copyright-ww">© 2018 by <a href="http://www.werbewind.com/" target="_blank" rel="noopener">Werbewind</a> <span class="desk">- die Agentur</span><span class="werbewind-icon sprite"></span></div>
      </div>
    </div>

    
    
    
    <?php
    include 'includes/picgallery-xxl.php';
    ?>
    

    <!-- build:js /scripts/vendor.js -->
    <!-- bower:js -->
    <script src="../../bower_components/jquery/dist/jquery.js"></script>
    
    
       <script src="../../bower_components/aos/dist/aos.js"></script>
       
    <script src="../../bower_components/pickadate/lib/picker.js"></script>
    <script src="../../bower_components/pickadate/lib/picker.date.js"></script>
    
    
    <script src="../../bower_components/flexslider/jquery.flexslider.js"></script>
        
    <script src="../../bower_components/lightgallery/dist/js/lightgallery-all.js"></script>
    <script src="../../bower_components/unveil/jquery.unveil.js"></script>
    <script src="../../bower_components/jquery-form-validator/form-validator/jquery.form-validator.js"></script>
    <!-- endbower -->
    <!-- endbuild -->
    <!-- build:js({app,.tmp}) /scripts/main.js -->
    <script src="/scripts/main.js"></script>
    <!-- endbuild -->

<?php
$REDCMS_TPL->printBodyEnd();

if ($redcmssettings['analytics'] == 1 && !empty($redcmssettings['analyticscode'])) {
    echo $redcmssettings['analyticscode'];
}

if ($redcmssettings['cookie_info'] == 1) {
    $cookie = new CookieInfo_Main;
    echo $cookie->getOutputCode();
}
?>
</div>
  </body>
</html>
