/* global Modernizr, picgalleryXxlPics, AOS */
'use strict';

/*jshint -W062 */
window.requestAnimFrame = function () {
    return (
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (/* function */ callback) {
                window.setTimeout(callback, 1000 / 60);
            }
    );
}();

function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}
var bie = detectIE();

// Add class "scrolled" to <body> element if user has scrolled
// This is mainly used for reducing the height of the navigation and the logo,
// because the navigation should be in a fixed position
function registerScrolling() {
    var s = $(window).scrollTop();
    if (s > 38 && !$('body').hasClass('scrolled')) {
        $('body').addClass('scrolled');
    } else if (s < 38 && $('body').hasClass('scrolled')) {
        $('body').removeClass('scrolled');
    }
    if (bie === false) {
        $('.no-touchevents .flexslider').css('top', Math.round(s * 0.7));
        $('.no-touchevents .flexslider-pinsel').css('bottom', Math.round(s / 3.5));
        //$('.no-touchevents .content-wrapper .moving-elements').css('margin-top', Math.round(s / 2));
    }
}

(function animationLoop() {
    window.requestAnimFrame(animationLoop);
    registerScrolling();
})();

$('img[data-src]').unveil();

$('.no-touchevents .home #img-gallery').closest('div.cols').append('<a href="/news-pics/bildergalerie#c" class="home-bildgallery"><span class="gallerytext-wrap"><span class="bildgallery-icon sprite"></span><span class="gallery-text">mehr pics</span></span><span class="gallery-pinsel" data-aos="fade-down-left"></span></a>');

setTimeout(function () {
    AOS.init();
}, 1000);


$('a[data-lightbox="true"]').on('click', function (e) {
    var galleryItems = [];
    e.preventDefault();
    var image = $(this).attr('href');
    var currentImage = 0;
    var i = 0;
    $('a[data-lightbox="true"]').each(function () {
        if (image === $(this).attr('href')) {
            currentImage = i;
        }
        galleryItems.push({
            src: $(this).attr('href'),
        });
        i++;
    });

    $(this).lightGallery({
        dynamic: true,
        dynamicEl: galleryItems,
        index: currentImage,
        download: false,
        thumbnail: false
    });
});

$('a[data-lightbox="iframe"]').lightGallery({
    selector: 'this',
    width: '80%',
    download: false,
    counter: false
});


if ($('#kontaktform').length > 0) {
    $.validate({
        form: '#kontaktform',
        borderColorOnError: ''
    });
}


if (!Modernizr.inputtypes.date && $('input[type="date"]').length > 0) {
    $.extend($.fn.pickadate.defaults, {
        labelMonthNext: 'Nächster Monat',
        labelMonthPrev: 'Vorheriger Monat',
        labelMonthSelect: 'Monat wählen',
        labelYearSelect: 'Jahr wählen',
        monthsFull: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        weekdaysShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        firstDay: 1,
        today: 'heute',
        clear: 'löschen',
        close: 'schließen',
        format: 'dd.mm.yyyy',
        formatSubmit: 'dd.mm.yyyy'
    });
    $('input[type="date"]').pickadate({
        min: new Date()
    });
}
function initDatepicker(f1, f2) {
    if (Modernizr.inputtypes.date) {
        $('#' + f1).on('change', function () {
            var date = new Date($(this).val());
            date.setDate(date.getDate() + 7);
            var departure = date.getFullYear() + '-' + (('0' + (date.getMonth() + 1)).slice(-2)) + '-' + (('0' + date.getDate()).slice(-2));
            $('#' + f2).val(departure);
        });
        return;
    }

    var $arrival = $('#' + f1).pickadate({
        min: new Date()
    });
    var $departure = $('#' + f2).pickadate({
        min: new Date()
    });

    var picker1 = $arrival.pickadate('picker');
    if (picker1 === undefined) {
        return;
    }
    var picker2 = $departure.pickadate('picker');
    picker1.on('set', function () {
        var arrival = picker1.get('select');
        if (arrival !== null) {
            picker2.set({
                'select': new Date(arrival.year, arrival.month, (arrival.date + 7)), // Add one week
                'min': new Date(arrival.year, arrival.month, (arrival.date + 2))  // Set new min date
            });
        }
    });
}
if ($('#sa_anreise').length > 0) {
    initDatepicker('sa_anreise', 'sa_abreise');
}
if ($('#anreise').length > 0) {
    initDatepicker('anreise', 'abreise');
}



// Flexslider init
if ($('#flexslider').length > 0) {

    $('#flexslider').flexslider({
        animation: 'fade',
        prevText: '',
        nextText: '',
        slideshowSpeed: 5500,
        animationSpeed: 4000,
        controlNav: true,
        directionNav: false,
        start: function () {
            // resolve the bug how not show the true image width
            $(window).trigger('resize');
        }
    });
}

// Flexslider Detail init
if ($('.tool-changer').length > 0) {
    $('.tool-changer').each(function () {
        var changer = $(this);
        var images = changer.data('pics');

        for (var i = 0; i < images.length; i++) {
            changer.find('.slides').append('<li data-src="' + images[i] + '"><img src="' + images[i] + '" alt=""></li>');
        }

        changer.flexslider({
            animation: 'fade',
            prevText: '',
            nextText: '',
            slideshowSpeed: 3500,
            animationSpeed: 2000,
            controlNav: true,
            directionNav: true,
            start: function () {
                // resolve the bug how not show the true image width
                $(window).trigger('resize');
            }
        });
        if ($('.button-grundriss').length > 0) {
            changer.find('.slides').append('<li class="grundriss" style="display: none;" data-src="' + $('.button-grundriss').attr('href') + '"><img src="' + $('.button-grundriss').attr('href') + '" alt=""></li>');
        }
        $('.tool-changer .slides').lightGallery({
            thumbnail: false,
            download: false
        });
        $('.button-grundriss').on('click', function (e) {
            e.preventDefault();
            changer.find('.grundriss').trigger('click');
        });
    });
}



var currentURL = location.pathname;
var f = true;
$('a[href^="/bildergalerie"]').on('click', function (e) {
    e.preventDefault();

    var url = $(this).attr('href');
    f = false;
    if (Modernizr.history) {
        history.pushState(null, null, url);
    }

    var data = picgalleryXxlPics,
            start = $(this).data('start');
    var lg = $(this).lightGallery({
        dynamic: true,
        html: true,
        loop: true,
        dynamicEl: data,
        index: start,
        download: false,
        toogleThumb: false
    });

    lg.on('onAfterSlide.lg onSlideItemLoad.lg', function () {
        if ($('.lg-current img').get(0).complete) {
            $('.lg-sub-html').css({
                'width': $('.lg-current .lg-image').width() + 8,
                'top': $('.lg-current .lg-image').position().top
            });
        }
    });
    lg.on('onBeforeClose.lg', function () {
        if (Modernizr.history) {
            history.pushState(null, null, currentURL);
        } else {
            location.href = currentURL;
        }
    });
    $(window).on('resize', function () {
        $('.lg-sub-html').css({
            'width': $('.lg-current img').width() + 8,
            'top': $('.lg-current img').position().top
        });
    });
});
window.onpopstate = function () {
    if (f === false) {
        location.href = location.pathname;
    }
};


$('.footer-box-heading').on('click', function () {
    if ($('.footer-box-content:visible').length > 1) {
        return false;
    }
    var n = $(this).next();
    if (n.is(':visible')) {
        n.slideUp();
        $(this).removeClass('active');
        return;
    }
    $('.footer-box-content:visible').slideUp().prev().removeClass('active');
    n.slideToggle().prev().toggleClass('active');
});


// Show sub menu with JS if touch device
$(' #navi li:has(ul) > a').on('click', function (e) {
    e.preventDefault();
    if ($(this).parent().find('ul').is(':visible')) {
        $(this).parent().find('ul').slideUp();
    } else {
        if ($(' #navi ul li > ul').is(':visible')) {
            $(' #navi ul li > ul').slideUp();
        }
        $(this).parent().find('ul').slideDown();
    }




    // $(this).parent().find('ul').slideToggle();

});
// Show/hide mobile menu
$('.navi-icon-wrap').on('click', function () {
    $('#navi-icon ').toggleClass('active');
    $('#navi .ul-wrap').toggleClass('active');
});


$('.no-touchevents #navi li:has(ul) > a').on('click', function (e) {
    e.preventDefault();
});

// Change title if tab/window is inactive
var title = $('head title').html();
$(window).on('blur', function () {
    $('head title').html('Musikschule Kleinwalsertal');
}).on('focus', function () {
    if ($('head title').html() === 'Musikschule Kleinwalsertal') {
        $('head title').html(title);
    }
});


$('input[name="anmeldung"]').on('change', function () {
    var meldung = $(this).val();
    if (meldung === 2) {
        $('#previous-ins').toggleClass('hide');
    }
});

$('#zahlart_1,#zahlart_2').on('click', function () {
    var elId = $(this).attr('id');
    var colId = '#' + elId + '-field';
    if ($('#zahlart_2-field').is(':visible')) {
        $('#zahlart_2-field').slideUp();
    }
    $(colId).slideDown();
});
