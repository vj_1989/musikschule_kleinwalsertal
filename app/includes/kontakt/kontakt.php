<?php
global $staticlinks;
?>
<div id="kontakt" class="contactform">
<?php
if ($contactform_sent) {
  echo '<p class="success">'.LANG_CONTACT_SUCCESS.'</p>';
}
else {
  if (!empty($CF->messages)) {
    echo '<p class="error">'.LANG_CONTACT_ERROR.'</p>';
    echo '<p class="error">- '.implode('<br />- ', $CF->messages).'</p>';
  }
?>
<form id="kontaktform" name="kontaktform" method="post" <?php
if(!defined('INCLUDED_VIA_FACEBOOK')){
  echo 'action="'.$staticlinks['kontakt']['url'].'-gesendet#c"';
}
?>>
  <input type="hidden" name="kkzeit" value="<?php echo CF_TIME; ?>">
<?php
if(defined('INCLUDED_VIA_FACEBOOK') && isset($fb_id)){
  echo '<input type="hidden" name="fb_id" value="'.$fb_id.'">';
}
?>
  <hr>
  <p><?php #echo LANG_CONTACT_TEXT; ?></p>
  <p><?php #echo LANG_CONTACT_RRV; ?></p>
  
  <div class="text-ssl">
    <div class="contact-lock"><div class="keyhole"></div></div>
    <?php echo str_replace('{{impressum}}', ROOT.$staticlinks['impressum']['url'].'#datenschutz', LANG_CONTACT_PRIVACY); ?>    
  </div>

  <div class="cols cols-form">
    <div class="contactform-rb">
    <?php
    foreach($CF_SETTINGS['salutation'] as $i => $s){
      echo $FORM->createField(
        'anrede',
        'anrede_'.$i,
        'radio',
        $CF_FIELDS['anrede']['value'],
        $i,
        '',
        ($CF_FIELDS['anrede']['pflicht'] ? ' data-validation="required"' : '')
      );
    ?>
      <label for="anrede_<?php echo $i; ?>"><?php echo $s['feld']; if($s['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php } ?>
    </div>
  </div>

  <div class="cols cols-form">
    <div class="col-1">
      <label for="vorname"><?php echo $CF_FIELDS['vorname']['feld']; if($CF_FIELDS['vorname']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
      <?php
      echo $FORM->createField(
        'vorname',
        '',
        'text',
        $CF_FIELDS['vorname']['value'],
        '',
        '',
        ($CF_FIELDS['vorname']['pflicht'] ? ' data-validation="required"' : '')
      );
      ?>
    </div>
    <div class="col-2">
      <label for="nachname"><?php echo $CF_FIELDS['nachname']['feld']; if($CF_FIELDS['nachname']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
      <?php
      echo $FORM->createField(
        'nachname',
        '',
        'text',
        $CF_FIELDS['nachname']['value'],
        '',
        '',
        ($CF_FIELDS['nachname']['pflicht'] ? ' data-validation="required"' : '')
      );
      ?>
    </div>
  </div>

  <div class="cols cols-form">
    <div class="col-1">
      <label for="email"><?php echo $CF_FIELDS['email']['feld']; if($CF_FIELDS['email']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
      <?php
      echo $FORM->createField(
        'email',
        '',
        'text',
        $CF_FIELDS['email']['value'],
        '',
        '',
        ($CF_FIELDS['email']['pflicht'] ? ' data-validation="email"' : '')
      );
      ?>
      <div id="kkemail"><label for="email2">Email (bitte dieses Feld leer lassen)</label><input id="email2" name="email2" type="email" value="" /></div>
    </div>
  </div>

  <div class="cols cols-form">
      <div class="col-1">
        <label for="telefon"><?php echo $CF_FIELDS['telefon']['feld']; ?></label>
        <?php
        echo $FORM->createField(
          'telefon',
          '',
          'text',
          $CF_FIELDS['telefon']['value']
        );
        ?>
      </div>
      <div class="col-2">
        <label for="strasse"><?php echo $CF_FIELDS['strasse']['feld']; ?></label>
        <?php
        echo $FORM->createField(
          'strasse',
          '',
          'text',
          $CF_FIELDS['strasse']['value']
        );
        ?>
      </div>
    </div>	

    <div class="cols cols-form">
      <div class="col-1">
        <label for="plz"><?php echo $CF_FIELDS['plz']['feld']; ?></label>
        <?php
        echo $FORM->createField(
          'plz',
          '',
          'text',
          $CF_FIELDS['plz']['value']
        );
        ?>
      </div>
      <div class="col-2">
        <label for="ort"><?php echo $CF_FIELDS['ort']['feld']; ?></label>
        <?php
        echo $FORM->createField(
          'ort',
          '',
          'text',
          $CF_FIELDS['ort']['value']
        );
        ?>
      </div>
    </div>

  <?php
  /*<div class="cols cols-form">
    <div class="contactform-cb">
      <input type="checkbox" id="newsletter" name="newsletter" value="1"<?php if ($CF_FIELDS['newsletter']['value'] == '1') { echo ' checked="checked"'; } ?> />
      <label for="newsletter"><?php echo $CF_FIELDS['newsletter']['feld']; ?></label>
    </div>
  </div>*/
  ?>

  <div class="cols-form">
    <label for="nachricht"><?php echo $CF_FIELDS['nachricht']['feld']; if($CF_FIELDS['nachricht']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php
    echo $FORM->createField(
      'nachricht',
      '',
      'textarea',
      $CF_FIELDS['nachricht']['value'],
      '',
      '',
      ($CF_FIELDS['nachricht']['pflicht'] ? ' data-validation="required"' : '')
    );
    ?>	
  </div>



  <hr>

  <div class="buttons">
    <button name="kontaktform_send" type="submit" class="button-kontakt btn-arrow sprite-after"><?php echo LANG_CONTACT_SUBMIT; ?></button>
  </div>

</form>
<?php
}
?>
</div>