<?php
global $staticlinks;
?>

<span class="parallex-pinsel"  data-aos="fade-right"></span>
<div class="parallex-container">
	<div class="parallex-wrapper">
		<div class="parallex-block blk1">
			<a href="<?php echo $staticlinks['unterrichtsangebot']['url']; ?>#c" class="parallex-link">
            	<span class="parallex-icon sprite-before"></span>
                <span class="parallex-text">Unser Unterrichtsangebot</span>
            </a>
		</div>
    <div class="parallex-block blk2">
		<a href="<?php echo $staticlinks['wajuba']['url']; ?>#c" class="parallex-link">
            	<span class="parallex-icon sprite-before"></span>
                <span class="parallex-text">WAJUBA</span>
            </a>
	</div>
    <div class="parallex-block blk3">
<a href="<?php echo $staticlinks['events']['url']; ?>#c" class="parallex-link">
            	<span class="parallex-icon sprite-before"></span>
                <span class="parallex-text">Veranstaltungen</span>
            </a>
	</div>

	</div>
</div>
