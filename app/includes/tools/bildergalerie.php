<?php
// @todo use class of login system

$PGXXL = new PicGalleryXxl_Main();

$PGXXL->setNewImgPath(IMG_PATH.'galleryxl/');

$album = $PGXXL->getAlbum(0);
$pics  = $PGXXL->getPictures(0);
$data  = array();

if (!empty($pics)) {
  $content = '';
  foreach($pics as $pic) {
    $title = KUNDEN_BETRIEB;
    if (!empty($pic['titel'])) {
      $title .= ' - '.$pic['titel'];
    }
    $data[] = array(
      'thumb'   => str_replace('/xl/', '/m/', $pic['bild']),
      'src'     => $pic['bild'],
      'subHtml' => '<div class="customHtml"><h4>'.$title.'</h4><p>'.(!empty($pic['copyright']) ? '&copy; '.$pic['copyright'] : '').'</p></div>',
      'title'   => $title
    );
  }
  echo '<script>var picgalleryXxlPics='.json_encode($data).'</script>';

  $content = '';

  if (isset($picgalleryxxl_fallback)) {

    $content .= '<ul class="picgallery-xxl-alternative">';

    $i = 0;

    foreach($pics as $pic) {

      $i++;

      $title = $pic['titel'];

      if (!empty($pic['copyright'])) {
        if (!empty($title)) {
          $title .= ' | ';
        }
        $title .= '&copy; '.$pic['copyright'];
      }
      $content .= '<li><div id="pic-'.$i.'" class="anchor"></div><figure><img src="'.$pic['bild'].'" alt=""><figcaption>'.$title.'</figcaption></figure></li>';

    }

    $content .= '</ul>';

  }
  else {

    $content .= '<div class="gallerybox-wrapper">';
    $content .= '<div class="gallerybox">';

    $i = 0;

    foreach($data as $pic) {

      $content .= '<a class="gallery-start" href="'.$staticlinks['bildergalerie']['url'].'#pic-'.($i+1).'" data-slide="'.$i.'"><img src="'.$pic['thumb'].'" alt=""></a>';

      $i++;

      if ($i >= 5) {
        break;
      }
    }

    $content .= '</div>'; # /.gallerybox

    $content .= '<div class="gallerybox-more">';
    $content .= '<a class="gallery-start" href="'.$staticlinks['bildergalerie']['url'].'#c">'.LANG_VIEW_GALLERY.'</a>';
    $content .= '</div>';

    $content .= '</div>'; # /.gallerybox-wrapper

  }
  echo $content;
}
?>