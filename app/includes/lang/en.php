<?php
define('LANG_ACTION_MAIL',    'Contact us');
define('LANG_ACTION_VOUCHER', 'Vouchers');

define('LANG_INQUIRE', 'inquire');

define('LANG_CONTACT',               'Contact');
@define('LANG_CONTACT_TEXT',				 'Do you have any questions, wishes or want to know if any room is available at a certain time? Then use this formular, we´ll give you an answer as soon as possible!');
define('LANG_CONTACT_RRV',           'We recommend to take an insurance in case of <a href="https://www.elviab2b.de/elvia/booking/eltravag.nsf/ElviaBookEntryTA?OpenForm&amp;Error=9001&amp;AN=HTL508&amp;PF=000000000000010&amp;PT=RRHOT" target="_blank" rel="noopener">travel cancellation</a>.');
define('LANG_CONTACT_PRIVACY',       'Your data are safe, because we use „https“. Please look at our <a href="{{impressum}}">privacy notice. </a>.');
@define('LANG_CONTACT_MORE_DATA',    'We wolud like to have some more data to contact you. We will not use your data for different marketing terms. ');
define('LANG_CONTACT_YOURDATA',      'Your data');
define('LANG_CONTACT_SALUTATION',    'Salutation');
define('LANG_CONTACT_SALUTATION_MS', 'Ms');
define('LANG_CONTACT_SALUTATION_MR', 'Mr');
define('LANG_CONTACT_SALUTATION_F',  'Family');
define('LANG_CONTACT_SALUTATION_C',  'Company');
define('LANG_CONTACT_FIRSTNAME',     'First name');
define('LANG_CONTACT_LASTNAME',      'Last name');
define('LANG_CONTACT_COMPANY',       'Company');
define('LANG_CONTACT_COUNTRY',       'Country');
define('LANG_CONTACT_STREET',        'Street');
define('LANG_CONTACT_HOUSENR',       'House number');
define('LANG_CONTACT_POSTCODE',      'Post code');
define('LANG_CONTACT_CITY',          'City');
define('LANG_CONTACT_PHONE',         'Phone');
define('LANG_CONTACT_MOBILE',        'Mobile');
define('LANG_CONTACT_EMAIL',         'Email');
define('LANG_CONTACT_YOURWISHES',    'Your wishes');
@define('LANG_CONTACT_ACCOMMODATION', 'Accommodation');
@define('LANG_CONTACT_PERSONS',       'Persons');
define('LANG_CONTACT_ARRIVAL',        'Arrival');
define('LANG_CONTACT_DEPARTURE',      'Departure');
@define('LANG_CONTACT_ACCOMMODATION', 'Accommodation');
define('LANG_CONTACT_ADULTS',         'Adults');
define('LANG_CONTACT_CHILDRENAGE',    'Age of child');
define('LANG_CONTACT_NEWSLETTER',     'Newsletter - If you want to receive our newsletter your address will be used for internal advertising only. You can stop receiving the newsletter at any time.');
define('LANG_CONTACT_MESSAGE',        'Your message');
define('LANG_CONTACT_SUBMIT',         'Send');
define('LANG_CONTACT_SUBMIT2',        'submit <strong>request</strong>');
@define('LANG_CONTACT_OPTIONAL_DATA', 'optional data');
define('LANG_CONTACT_PERSONS',        'Persons');
define('LANG_CONTACT_DAYS',           'Days');
define('LANG_CONTACT_MISC',           'Extras');
@define('LANG_CONTACT_EXPRESSREQUEST','Expressrequest');
define('LANG_CONTACT_REFERRER',       'Please inform us as to how you became aware of our homepage. Thank you.');
define('LANG_CONTACT_SUCCESS',        'Thanks for your request. We will get in contact with you soon.');
define('LANG_CONTACT_NL_SUCCESS',     'Thanks for ordering our newsletter. You will get an email. You will have to click a link in that email in order to confirm your subscription.');
@define('LANG_CONTACT_ERROR',		      'Request could not be sent. The following information is incorrect:');
@define('LANG_NEWSLETTER_ERROR',	    'Registration could not be carried out. The following information is incorrect:');
define('LANG_CONTACT_MANDATORY', 	    'Mandatory');

define('LANG_FOOTER_CONTACT_PHONE', 'Phone');
define('LANG_FOOTER_CONTACT_SEND',  'Contact us');

define('LANG_FOOTER_IMPRINT', 'Imprint');
define('LANG_FOOTER_PRIVACY', 'Privacy');
define('LANG_FOOTER_LEGAL',   'Legal notice');

define('LANG_BOX_WEATHER',       'Weather');
define('LANG_BOX_WEATHER_TODAY', 'Today');
define('LANG_BOX_WEATHER_MORE',  'Weather forecast');

define('LANG_BOX_FACEBOOK',         'Facebook');
@define('LANG_BOX_FACEBOOK_FAN',    'Visit our Facebook page');
@define('LANG_BOX_FACEBOOK_BUTTON', 'Visit us');

define('LANG_WEATHER_HEADING',           'Current weather for');
define('LANG_WEATHER_SUBHEADING',        'Weatherforecast for the next %s days.');
define('LANG_WEATHER_ZUSTAND',           'Weather situation');
define('LANG_WEATHER_TODAY',             'Today');
define('LANG_WEATHER_MORGENS',           'Morning');
define('LANG_WEATHER_MITTAGS',           'Noon');
define('LANG_WEATHER_ABENDS',            'Evening');
define('LANG_WEATHER_TEMPERATUR',        'Temperature');
define('LANG_WEATHER_NIEDERSCHL',        'Chance of rain ');
define('LANG_WEATHER_WINDGESCHW',        'Wind velocity');
define('LANG_WEATHER_WINDRICHTUNG',      'Wind direction');
define('LANG_WEATHER_BEDECKT',           'Coated');
define('LANG_WEATHER_LEICHTES_GEWITTER', 'Light storm');
define('LANG_WEATHER_GEWITTER',          'Storm');
define('LANG_WEATHER_LEICHTBEWOELKT',    'Partly cloudy');
define('LANG_WEATHER_NEBEL',             'Fog');
define('LANG_WEATHER_REGEN',             'Rain');
define('LANG_WEATHER_LEICHTER_REGEN',    'Light rain');
define('LANG_WEATHER_SCHAUER',           'Shower');
define('LANG_WEATHER_SCHNEEFALL',        'Snowfall');
define('LANG_WEATHER_SCHNEESCHAUER',     'Snow shower');
define('LANG_WEATHER_SONNIG',            'Sunny');
define('LANG_WEATHER_SPRUEHREGEN',       'Drizzle');
define('LANG_WEATHER_WOLKIG',            'Cloudy');

define('LANG_MONDAY',    'Monday');
define('LANG_TUESDAY',   'Tuesday');
define('LANG_WEDNESDAY', 'Wednesday');
define('LANG_THURSDAY',  'Thursday');
define('LANG_FRIDAY',    'Friday');
define('LANG_SATURDAY',  'Saturday');
define('LANG_SUNDAY',    'Sunday');

define('LANG_ROUTE_HEADING', 'Calculate route with Google-Maps');
define('LANG_ROUTE_LEGEND',  'Your location');
define('LANG_ROUTE_SUBMIT',  'Show driving plan ');

define('LANG_BOX_BERGBAHN',      'Summer railway ticket');
define('LANG_BOX_BERGBAHN_TEXT', 'Exclusive for our guests – moutain railway ticket included from May to November!');
define('LANG_BOX_BERGBAHN_MORE', 'more info');
define('LANG_BOOK',              'book');
define('LANG_BOX_HOLIDAYCHECK',  'Holiday<br>Check');
?>