<?php
$P3 = new PicGallery3_Main();
$g3_cats = $P3 ->getCategories();

echo '<div class="grallery-wrap">';
foreach($g3_cats as $g3_cat){
	echo '<div class="gallery-cat-wrap">';
	echo '<div class="gallery-cat-head"><h2>'.$g3_cat['titel'] .'</h2></div>';
	
$g3_galleries = $P3->getGalleries($g3_cat['id'], true, 'id');
	
	echo '<div class="gallery-pics-wrap">';
		foreach($g3_galleries as $g3_gallery){
			foreach($g3_gallery['pics'] as $g3_gallerypic){
			echo '<div class="gallery-pic">';
			echo '<a href="'.IMG_PATH.'gallery/'.$g3_gallerypic['bild'].'" class="gallery-link" data-lightbox="true"> <img src="'.IMG_PATH.'gallery/'.$g3_gallerypic['bild'].'" /></a>';
			echo '</div>';
			}
			echo '</div>';
		}
	echo '</div>';

}
echo '</div>';
/*
echo '<pre>';
var_dump($g3_galleries);
echo '</pre>';*/

 ?>