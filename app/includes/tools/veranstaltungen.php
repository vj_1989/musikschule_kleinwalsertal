<?php
global $staticlinks,$event;

if (!$WW_EVENTS instanceof Events_Main) {
    $params = array(
        'results_per_page' => 15,
        'in_house_only'    => false
    );
    $WW_EVENTS = new Events_Main($params);
}

// @todo replace Lib_Strings and Lib_Dates with login classes

$qs = '?';

if (!empty($_GET['es_from'])) {
  $qs .= 'es_from='.$_GET['es_from'];
}

if (!empty($_GET['es_to'])) {
  if (strlen($qs) > 1) {
    $qs .= '&amp;';
  }
  $qs .= 'es_to='.$_GET['es_to'];	
}

if (!empty($_GET['es_search_string'])) {
  if (strlen($qs) > 1) {
    $qs .= '&amp;';
  }
  $qs .= 'es_search_string='.$_GET['es_search_string'];
}

if (!empty($_GET['es_date'])) {
  if (strlen($qs) > 1) {
    $qs .= '&amp;';
  }
  $qs .= 'es_date='.$_GET['es_date'];
}

$content = '';

// If event details are fetched (includes/init.php)
if (!empty($event) && intval($event['event_id']) > 0) {

  $e = $event;
 
  if ( strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) != false ) {
    $back = '<a class="button back" href="'.$_SERVER['HTTP_REFERER'].'#e'.$e['event_id'].'">zurück</a>';
  }
  else {
    $back = '<a class="button back" href="/aktuelles/veranstaltungen'.$qs.'#e'.$e['event_id'].'">zurück</a>';
  }
  $content .= '<div class="cols cols-txt pb-medium"><div><h1>'.$e['titel'].'</h1></div></div>';  
  $content .= '<div class="clearfix event">';
  $content .= '<div class="event-back event-back-top">'.$back.'</div>';
  #$content .= '<h1 class="event-title">'.$e['titel'].'</h1>';
  $content .= '<p class="event-subtitle">';

  if ($e['ende_f'] != '' && $e['ende_f'] != '00.00.0000' && $e['ende_f'] != $e['start_f']) {
    $content .= 'vom '.$e['start_f'].' bis zum '.$e['ende_f'];
  }
  else {
    $content .= 'am '.$e['start_f'];
  }

  if ($e['zeit'] != '') {

    if (substr_count($e['zeit'], 'bis') == 1) {
      $content .= ', von '.$e['zeit'].' Uhr';
    }
    else {
      $content .= ', um '.$e['zeit'].' Uhr';
    }

  }

  if ($e['ort'] != '' || $e['ort2'] != '') {

    if ($e['ort'] != '') {

      $content .= ' in '.$e['ort'];

      if ($e['ort2'] != '') {
        $content .= ' - '.$e['ort2'];
      }

    }
    else {
      $content .= ' in '.$e['ort2'];
    }

  }

  $content .= '</p>';
  $content .= '<div class="event-content clearfix">';

  $e_pics = array();
  $g      = '';

  if($e['gruppe'] > 0) {
    $g = 'g';
  }

  for($x=1; $x <= 4; $x++){

    if(!empty($e[$g.'bild'.$x]) && $e[$g.'bild'.$x.'_status'] == 1) {
      $e_pics[] = array(
        'src'  => $e[$g.'bild'.$x],
        'text' => $e[$g.'bild'.$x.'_text']
      );
    }

  }

  $pc  = count($e_pics);
  $pic = 1;

  if (isset($_GET['bild'])) {

    $pic = (int)$_GET['bild'];

    if(!in_array($pic, range(1, $pc))) {
      $pic = 1;
    }

  }

  if ($pc > 0) {

    $content .= '<div class="event-pic">';
    $content .= '<figure id="event-pic">';
    $content .= '<img src="'.$WW_EVENTS->img_path_abs.$e_pics[$pic-1]['src'].'" alt="">';
    $content .= '<figcaption>'.$e_pics[$pic-1]['text'].'</figcaption>';
    $content .= '</figure>';

    if ($pc > 1) {

      $content .= '<div id="event-thumbs" class="event-thumbs">';

      $x = 1;

      foreach($e_pics as $e_pic){

        $content .= '<a href="?event='.$e['event_id'].'&amp;bild='.$x.'#event">';
        $content .= '<img';
        if($x == $pic){
          $content .= ' class="current"';
        }
        $content .= ' src="'.$WW_EVENTS->img_path_abs.$e_pic['src'].'" alt="'.htmlspecialchars($e_pic['text']).'">';
        $content .= '</a>';

        $x++;

      }

      $content .= '</div>'; # /.event-thumbs

    }

    $content .= '</div>'; # /.event-pic

  }

  if ($e['abgesagt'] == 1 && !empty($e['abgesagt_text'])) {
    $content .= '<div class="event-warning">'.$e['abgesagt_text'].'</div>';
  }
  else if ($e['ausverkauft'] == 1) {
    $content .= ' <div class="event-warning">Diese Veranstaltung ist bereits ausverkauft!</div>';
  }
  else if ($e['verlegt'] == 1) {
    $content .= ' <div class="event-warning">Diese Veranstaltung wurde auf ein anderes Datum verschoben!</div>';
  }

  if (!empty($e['beschreibung'])) {
    $content .= '<div class="event-text">'.$e['beschreibung'].'</div>';
  }

  $location_id  = intval($e['veranstaltungsort_id']);
  $organizer_id = intval($e['veranstalter_id']);

  if ($location_id > 0 || $organizer_id > 0) {

    $location = '<h2>Veranstaltungsort</h2>';
    $location .= '<p>';

    if(!empty($e['ort_website'])) {
      $location .= '<a href="http://'.$e['ort_website'].'" target="_blank" rel="noopener">';
    }

    if(!empty($e['ort_objekt'])) {
      $location .= $e['ort_objekt'];
    }

    if(!empty($e['ort_website'])) {
      $location .= '</a>';
    }

    if(!empty($e['ort_strasse'])) {
      $location .= '<br>'.$e['ort_strasse'];
    }

    if(!empty($e['ort_plz'])) {
      $location .= '<br>'.$e['ort_plz'];
    }

    if(!empty($e['ort_ort'])) {
      if(empty($e['ort_plz'])) {
        $location .= '<br>';
      }
      $location .= ' '.$e['ort_ort'];
    }

    if(!empty($e['ort_telefon'])) {
      $location .= '<br>'.$e['ort_telefon'];
    }

    if(!empty($e['ort_fax'])) {
      $location .= '<br>'.$e['ort_fax'];
    }

    if(!empty($e['ort_email'])) {
      $location .= '<br>'.Lib_Strings::noSpam($e['ort_email']);
    }

    $location .= '</p>';

    $organizer = '<h2>Veranstalter</h2>';
    $organizer .= '<p>';

    if(!empty($e['ver_website'])) {
      $organizer .= '<a href="http://'.$e['ver_website'].'" target="_blank" rel="noopener">';
    }

    if(!empty($e['ver_veranstalter'])) {
      $organizer .= $e['ver_veranstalter'];
    }

    if(!empty($e['ver_website'])) {
      $organizer .= '</a>';
    }

    if(!empty($e['ver_telefon'])) {
      $organizer .= '<br>'.$e['ver_telefon'];
    }

    if(!empty($e['ver_fax'])) {
      $organizer .= '<br>'.$e['ver_fax'];
    }

    if(!empty($e['ver_email'])) {
      $organizer .= '<br>'.Lib_Strings::noSpam($e['ver_email']);
    }

    $organizer .= '</p>';

    $content .= '<div class="clearfix event-info">';

    if ($location_id > 0 && $organizer_id > 0) {
      $content .= '<div class="event-location">'.$location.'</div>';
      $content .= '<div class="event-organizer">'.$organizer.'</div>';
    }
    else if($location_id > 0) {
      $content .= $location;
    }
    else if($organizer_id > 0) {
      $content .= $organizer;
    }

    $content .= '</div>';

  }

  $content .= '</div>'; # /.event-content
  $content .= '<div class="event-back event-back-bottom">'.$back.'</div>';
  $content .= '</div>'; # /.event

  $content .= '<script>';
  $content .= '$(document).ready(function(){';
  $content .= '$(\'#event-thumbs a\').on(\'click\',function(e){';
  $content .= 'e.preventDefault();';
  $content .= '$(\'#event-pic img\').attr(\'src\',$(this).find(\'img\').attr(\'src\'));';
  $content .= '$(\'#event-pic figcaption\').html($(this).find(\'img\').attr(\'alt\'));';
  $content .= '});';
  $content .= '});';
  $content .= '</script>';

}
else {

	$p      = 1;
  $s      = '';
  $m      = '';
  $from   = '';
  $from_f = '';
  $to     = '';
  $to_f   = '';
  if (isset($_GET['p'])) {
    $p = (int)$_GET['p'];
  }
  if (isset($_GET['s'])) {
    $s = trim(htmlspecialchars(strip_tags($_GET['s'])));
  }
  if (isset($_GET['m'])) {
    $m = trim(htmlspecialchars(strip_tags($_GET['m'])));
  }
  if(!empty($_GET['from'])) {
    $from   = date('Y-m-d', strtotime($_GET['from']));
		$from_f = date('d.m.Y', strtotime($_GET['from']));
  }
  if(!empty($_GET['to'])) {
    $to   = date('Y-m-d', strtotime($_GET['to']));
		$to_f = date('d.m.Y', strtotime($_GET['to']));
  }

  $events = $WW_EVENTS->getEvents($p, $m, $s, $from, $to, LANG);
	$content .= '<div class="cols cols-txt pb-medium"><div><h1>Unsere Veranstaltungen</h1></div></div>';
  $content .= '<div class="events">'; 
  
  $content .= '<div class="events-form clearfix">';
  $content .= '<form action="#c" method="get">';
  
  $content .= '<div class="clearfix events-form-fields">';
  
  $content .= '<div class="events-form-field events-form-field-from">';
  $content .= '<label for="events-from">von</label>';
  $content .= '<input id="events-from" name="from" type="date" value="'.$from.'">';
  $content .= '</div>'; # /.events-form-field

  $content .= '<div class="events-form-field events-form-field-to">';
  $content .= '<label for="events-to">bis</label>';
  $content .= '<input id="events-to" name="to" type="date" value="'.$to.'">';
  $content .= '</div>'; # /.events-form-field

  $content .= '<div class="events-form-field events-form-field-m">';
  $content .= '<label for="events-m">oder nach Monaten</label>';
  $content .= '<select id="events-m" name="m"><option value="">Alle Monate</option>';

  $months = $WW_EVENTS->getEventsMonths();

  foreach($months as $month){

    $t = explode('-', $month);

    $content .= '<option value="'.$month.'"';

    if($month == $m){
      $content .= 'selected="selected"';
    }

    $content .= '>'.Lib_Dates::getMonthName($t[1]).' '.$t[0].'</option>';

  }

  $content .= '</select>';
  $content .= '</div>'; # /.events-form-field

  $content .= '<div class="events-form-field events-form-field-s">';
  $content .= '<label for="events-s">Suchbegriff</label>';
  $content .= '<input id="events-s" name="s" value="'.$s.'" placeholder="Suchbegriff eingeben">';
  $content .= '</div>'; # /.events-form-field
    $content .= '<div class="events-form_buttons clearfix">';
  $content .= '<button type="submit" class="button btn-arrow sprite-after">Suche starten</button>';
  $content .= '</div>';
  $content .= '</div>'; # /.events-form-fields
          

  $content .= '</form>';
  $content .= '</div>'; # /.event-search-form

  if(!empty($s) || !empty($m) || (!empty($from) && !empty($to))) {

    $content .= '<div class="events-searched-for">';
    $content .= '<span class="events-searched-for-label">Sie haben gesucht nach:</span> ';

    $searched_for = array();

    if (!empty($s)) {
      $searched_for[] = '<span class="events-searched-for-value">'.$s.'</span>';
    }

    if (!empty($from) && !empty($to)) {
      $str = '<span class="events-searched-for-value">';
      $str .= Lib_Dates::getWeekdayName(date('N', strtotime($from)));
      $str .= ', '.$from_f.' bis ';
      $str .= Lib_Dates::getWeekdayName(date('N', strtotime($to)));
      $str .= ', '.$to_f;
      $str .= '</span>';
      $searched_for[] = $str;
    }
    else if (!empty($m)) {
      $str = '<span class="events-searched-for-value">';
      $str .= Lib_Dates::getMonthName(date('m', strtotime($m)));
      $str .= ' '.Lib_Dates::getMonthName(date('Y', strtotime($m)));
      $str .= '</span>';
      $searched_for[] = $str;
    }
    $content .= implode(' und ', $searched_for);
    $content .= '</div>'; # /.events-searched-for
  }

  if (!empty($events)) {

    $pages = ceil($WW_EVENTS->not_paged_count / $WW_EVENTS->results_per_page);
    $pager = '';
    
    if($pages > 1){

      $pager .= '<div class="events-pages">';

      for($x=1; $x <= $pages; $x++){

        if($x == $p) {
          $pager .= '<span class="events-pages-current">'.$x.'</span>';
        }
        else {

          $href = '?p='.$x;

          if(!empty($from)){
            $href .= '&amp;from='.$from;
          }

          if(!empty($to)){
            $href .= '&amp;to='.$to;
          }

          if(!empty($m)){
            $href .= '&amp;m='.$m;
          }

          if(!empty($s)){
            $href .= '&amp;s='.$s;
          }

          $pager .= '<a href="'.$href.str_replace('?', '&amp;', $qs).'#c">'.$x.'</a>';

        }

      }

      $pager .= '</div>'; # /.events-pages
      $content .= $pager;
    }

    $day = '';

    foreach($events as $e){

      #$href = Lib_Strings::makeURL($e['titel']).'-event-'.$e['event_id'].$qs.'#c';
	  
	  $href = 'event-'.$e['event_id'].$qs.'#c';

      $content .= '<div class="anchor" id="e'.$e['event_id'].'"></div>';

      if($e['ende'] > $e['start']){
        $content .= '<h2>Dauerveranstaltung</h2>';
      }
      else if ($day != $e['start']) {
        $content .= '<h2>'.Lib_Dates::getWeekdayName($e['wochentag']).', '.$e['start_f'].'</h2>';
        $day = $e['start'];
      }

      $content .= '<div class="clearfix events-event"';
      #$content .= ' itemprop="event" itemscope itemtype="http://schema.org/Event"';
      $content .= '>';

      $content .= '<div class="events-event-content">';

      $pic = '';
      $g   = '';

      if($e['gruppe'] > 0) {
        $g = 'g';
      }

      if(!empty($e[$g.'bild5']) && $e[$g.'bild5_status'] == 1){
        $pic = $e[$g.'bild5'];
      }
      else if(!empty($e[$g.'bild1']) && $e[$g.'bild1_status'] == 1){
        $pic = $e[$g.'bild1'];
      }

      if (!empty($pic)) {
        $content .= '<div class="events-event-pic">';
        $content .= '<a href="'.$href.'"><img src="'.$WW_EVENTS->img_path_abs.$pic.'" alt="'.htmlspecialchars($e['titel']).'"></a>';
        $content .= '</div>';
      }

      $content .= '<h3><a';
      #$content .= ' itemprop="url"';
      $content .= ' href="'.$href.'">'.$e['titel'].'</a></h3>';
      $content .= '<div class="events-event-info">';
      $content .= '<span';
      #$content .= ' itemprop="startDate" content="'.$e['start'].'"';
      $content .= '>'.Lib_Dates::getWeekdayName($e['wochentag']).', '.$e['start_f'].'</span>';

      if($e['ende'] > $e['start']){
        $content .= ' bis ';
        $content .= '<span';
        #$content .= ' itemprop="endDate" content="'.$e['ende'].'"';
        $content .= '>'.$e['ende_f'].'</span>';
      }
      else if(!empty($e['zeit'])) {
        $content .= ' um '.$e['zeit'].' Uhr';
      }

      if (!empty($e['ort'])) {
        $content .= ' in '.$e['ort'];
      }

      $content .= '</div>'; # /.events-event-info

      if ($e['abgesagt'] == 1) {
        $content .= '<div class="events-warning">Abgesagt!';
        if(!empty($e['abgesagt_text'])){
          $content .= '<br>'.$e['abgesagt_text'];
        }
        $content .= '</div>';
      }
      else if ($e['ausverkauft'] == 1) {
        $content .= '<div class="events-warning">Ausverkauft!</div>';
      }
      else if ($e['verlegt'] == 1) {
        $content .= '<div class="events-warning">Verschoben!</div>';
      }
      $content .= '<p class="events-teaser">'.$WW_EVENTS->makeTeaser($e['beschreibung'], 170).'</p>';
      $content .= '<div class="events-more "><a href="'.$href.'" class="sprite-after button btn-arrow">mehr lesen</a></div>';
      $content .= '</div>'; # /.events-event-content

      $content .= '</div>'; # /.events-event

    }
    $content .= $pager;
  }
  else {
    $content .= '<p class="events-no-result">Es wurden keine Veranstaltungen gefunden.</p>';
  }

  $content .= '</div>'; # /.events
/*
<script>
// @todo this will throw an error as we include jQuery at the bottom of our page so it is not defined now
//       -> move this snippet to bottom and switch to pickadate for the datepicker
$(document).ready(function(){
  //initDatePicker('#events-from','#events-to');
});
</script>
*/
}

echo $content;
?>