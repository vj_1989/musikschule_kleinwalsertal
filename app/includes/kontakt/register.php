<?php
global $staticlinks;
?>
<div id="kontakt" class="contactform">
<?php
if ($contactform_sent) {
  echo '<p class="success">'.LANG_CONTACT_SUCCESS.'</p>';
}
else {
  if (!empty($CF->messages)) {
    echo '<p class="error">'.LANG_CONTACT_ERROR.'</p>';
    echo '<p class="error">- '.implode('<br />- ', $CF->messages).'</p>';
  }
?>
<form id="kontaktform" name="kontaktform" method="post" <?php
if(!defined('INCLUDED_VIA_FACEBOOK')){
  echo 'action="'.$staticlinks['register']['url'].'-gesendet#c"';
}
?>>
  <input type="hidden" name="kkzeit" value="<?php echo CF_TIME; ?>">
<?php
if(defined('INCLUDED_VIA_FACEBOOK') && isset($fb_id)){
  echo '<input type="hidden" name="fb_id" value="'.$fb_id.'">';
}
?>
  <hr>
  <p><?php #echo LANG_CONTACT_TEXT; ?></p>
  <p><?php #echo LANG_CONTACT_RRV; ?></p>

  <div class="text-ssl">
    <div class="contact-lock"><div class="keyhole"></div></div>
    <?php echo str_replace('{{impressum}}', ROOT.$staticlinks['impressum']['url'].'#datenschutz', LANG_CONTACT_PRIVACY); ?>
  </div>


  	<div class="cols cols-form">
    <div class="contactform-rb">
    <?php
    foreach($CF_SETTINGS['anmeldungsettings'] as $i => $s){
      echo $FORM->createField(
        'anmeldung',
        'anmeldung_'.$i,
        'radio',
        $CF_FIELDS['anmeldung']['value'],
        $i,
        '',
        ($CF_FIELDS['anmeldung']['pflicht'] ? ' data-validation="required"' : '')
      );
    ?>
      <label for="anmeldung_<?php echo $i; ?>"><?php echo $s['feld']; if($s['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php } ?>
    </div>
  </div>
  <div class="cols cols-form hide" id="previous-ins">
      <div class="col-1">
        <label for="previous_instrument_name"><?php echo $CF_FIELDS['previous_instrument_name']['feld']; ?></label>
        <?php
        echo $FORM->createField(
          'previous_instrument_name',
          '',
          'text',
          $CF_FIELDS['previous_instrument_name']['value']
        );
        ?>
      </div>
      </div>


<div class="cols cols-form">
      <div class="col-1">
        <label for="instrument_name"><?php echo $CF_FIELDS['instrument_name']['feld']; if($CF_FIELDS['instrument_name']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
        <?php
        echo $FORM->createField(
          'instrument_name',
          '',
          'text',
          $CF_FIELDS['instrument_name']['value'],
		   '',
		  '',
		  ($CF_FIELDS['nachname']['pflicht'] ? ' data-validation="required"' : '')
        );
        ?>
      </div>
      <div class="col-2">
        <label for="lehrerwunsch"><?php echo $CF_FIELDS['lehrerwunsch']['feld']; ?></label>
        <?php
        echo $FORM->createField(
          'lehrerwunsch',
          '',
          'text',
          $CF_FIELDS['lehrerwunsch']['value']
        );
        ?>
      </div>
    </div>


<?php /*?>  <div class="cols cols-form">
    <div class="contactform-rb">
    <?php
    foreach($CF_SETTINGS['salutation'] as $i => $s){
      echo $FORM->createField(
        'anrede',
        'anrede_'.$i,
        'radio',
        $CF_FIELDS['anrede']['value'],
        $i,
        '',
        ($CF_FIELDS['anrede']['pflicht'] ? ' data-validation="required"' : '')
      );
    ?>
      <label for="anrede_<?php echo $i; ?>"><?php echo $s['feld']; if($s['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php } ?>
    </div>
  </div><?php */?>

  <div class="cols cols-form">
    <div class="col-1">
      <label for="vorname"><?php echo $CF_FIELDS['vorname']['feld']; if($CF_FIELDS['vorname']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
      <?php
      echo $FORM->createField(
        'vorname',
        '',
        'text',
        $CF_FIELDS['vorname']['value'],
        '',
        '',
        ($CF_FIELDS['vorname']['pflicht'] ? ' data-validation="required"' : '')
      );
      ?>
    </div>
    <div class="col-2">
      <label for="nachname"><?php echo $CF_FIELDS['nachname']['feld']; if($CF_FIELDS['nachname']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
      <?php
      echo $FORM->createField(
        'nachname',
        '',
        'text',
        $CF_FIELDS['nachname']['value'],
        '',
        '',
        ($CF_FIELDS['nachname']['pflicht'] ? ' data-validation="required"' : '')
      );
      ?>
    </div>
  </div>

  <div class="cols cols-form">
    <div class="col-1">
      <label for="email"><?php echo $CF_FIELDS['email']['feld']; if($CF_FIELDS['email']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
      <?php
      echo $FORM->createField(
        'email',
        '',
        'text',
        $CF_FIELDS['email']['value'],
        '',
        '',
        ($CF_FIELDS['email']['pflicht'] ? ' data-validation="email"' : '')
      );
      ?>
      <div id="kkemail"><label for="email2">Email (bitte dieses Feld leer lassen)</label><input id="email2" name="email2" type="email" value="" /></div>
    </div>
     <div class="col-2">
        <label for="geburtsdatum"><?php echo $CF_FIELDS['geburtsdatum']['feld']; if($CF_FIELDS['geburtsdatum']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
        <?php
        echo $FORM->createField(
          'geburtsdatum',
          '',
          'text',
          $CF_FIELDS['geburtsdatum']['value'],
		  '',
		  '',
		  ($CF_FIELDS['geburtsdatum']['pflicht'] ? ' data-validation="required"' : '')
        );
        ?>
      </div>
  </div>

   <div class="cols cols-form">
      <div class="col-1">
        <label for="telefon"><?php echo $CF_FIELDS['telefon']['feld']; if($CF_FIELDS['telefon']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
        <?php
        echo $FORM->createField(
          'telefon',
          '',
          'text',
          $CF_FIELDS['telefon']['value'],
		   '',
		  '',
		  ($CF_FIELDS['telefon']['pflicht'] ? ' data-validation="required"' : '')
        );
        ?>
      </div>
      <div class="col-2">
        <label for="strasse"><?php echo $CF_FIELDS['strasse']['feld']; if($CF_FIELDS['strasse']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
        <?php
        echo $FORM->createField(
          'strasse',
          '',
          'text',
          $CF_FIELDS['strasse']['value'],
		   '',
		  '',
		  ($CF_FIELDS['strasse']['pflicht'] ? ' data-validation="required"' : '')
        );
        ?>
      </div>
    </div>

    <div class="cols cols-form">
      <div class="col-1">
        <label for="plz"><?php echo $CF_FIELDS['plz']['feld']; if($CF_FIELDS['plz']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
        <?php
        echo $FORM->createField(
          'plz',
          '',
          'text',
          $CF_FIELDS['plz']['value'],
		   '',
		  '',
		  ($CF_FIELDS['plz']['pflicht'] ? ' data-validation="required"' : '')
        );
        ?>
      </div>
      <div class="col-2">
        <label for="ort"><?php echo $CF_FIELDS['ort']['feld']; if($CF_FIELDS['ort']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
        <?php
        echo $FORM->createField(
          'ort',
          '',
          'text',
          $CF_FIELDS['ort']['value'],
		   '',
		  '',
		  ($CF_FIELDS['ort']['pflicht'] ? ' data-validation="required"' : '')
        );
        ?>
      </div>
    </div>

 <div class="cols cols-form ">
      <div class="col-1">
        <label for="erziehungsberechtigten">Name der Erziehungsberechtigten<br />
(bei Minderjährigen)</label>
        <?php
        echo $FORM->createField(
          'erziehungsberechtigten',
          '',
          'text',
          $CF_FIELDS['erziehungsberechtigten']['value']
        );
        ?>
      </div>
        <div class="col-2">
        <label for="geschwister">Sind bereits Geschwister an der
Musikschule angemeldet:
(bitte Namen eintragen)</label>
        <?php
        echo $FORM->createField(
          'geschwister',
          '',
          'text',
          $CF_FIELDS['geschwister']['value']
        );
        ?>
      </div>
      </div>


 <div class="cols cols-form ">
      <div class="col-1">
        <label for="nebenfach">Zusätzlich wird folgendes Nebenfach/Zusatzfach belegt:</label>
        <?php
        echo $FORM->createField(
          'nebenfach',
          '',
          'text',
          $CF_FIELDS['nebenfach']['value']
        );
        ?>
      </div>
      </div>

      <div class="cols cols-form">
    <div class="col-1">
     <label>wir benötigen ein Mietinstrument</label>
      <div class="contactform-rb">
    <?php
    foreach($CF_SETTINGS['mietinstrumentsettings'] as $i => $s){
      echo $FORM->createField(
        'mietinstrument',
        'mietinstrument_'.$i,
        'radio',
        $CF_FIELDS['mietinstrument']['value'],
        $i,
        '',
        ($CF_FIELDS['mietinstrument']['pflicht'] ? ' data-validation="required"' : '')
      );
    ?>
      <label for="mietinstrument_<?php echo $i; ?>"><?php echo $s['feld']; if($s['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php } ?>
    </div>
    </div>
<?php /*?>     <div class="col-2">
     <label>gewünschte Mietvariante</label>
      <div class="contactform-rb">
    <?php
    foreach($CF_SETTINGS['mietvariantesettings'] as $i => $s){
      echo $FORM->createField(
        'mietvariante',
        'mietvariante_'.$i,
        'radio',
        $CF_FIELDS['mietvariante']['value'],
        $i,
        '',
        ($CF_FIELDS['mietvariante']['pflicht'] ? ' data-validation="required"' : '')
      );
    ?>
      <label for="mietvariante_<?php echo $i; ?>"><?php echo $s['feld']; if($s['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php } ?>
    </div>
    </div><?php */?>
    </div>

    <div class="cols cols-form">
        <div class="col-1">
      <label>gewünschte Zahlungsart</label>
    <div class="contactform-rb">
    <?php
    foreach($CF_SETTINGS['zahlartsettings'] as $i => $s){
      echo $FORM->createField(
        'zahlart',
        'zahlart_'.$i,
        'radio',
        $CF_FIELDS['zahlart']['value'],
        $i,
        '',
        ($CF_FIELDS['zahlart']['pflicht'] ? ' data-validation="required"' : '')
      );
    ?>
    <label for="zahlart_<?php echo $i; ?>"><?php echo $s['feld']; if($s['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php } ?>
  </div>
  <div id="zahlart_2-field">
    <label for="iban">IBAN:</label>
      <?php
      echo $FORM->createField(
        'iban',
        'iban',
        'text',
        $CF_FIELDS['iban']['value']
      );
      ?>
  </div>

  </div>
  </div>


  <?php
  /*<div class="cols cols-form">
    <div class="contactform-cb">
      <input type="checkbox" id="newsletter" name="newsletter" value="1"<?php if ($CF_FIELDS['newsletter']['value'] == '1') { echo ' checked="checked"'; } ?> />
      <label for="newsletter"><?php echo $CF_FIELDS['newsletter']['feld']; ?></label>
    </div>
  </div>*/
  ?>

  <div class="cols-form">
    <label for="nachricht"><?php echo $CF_FIELDS['nachricht']['feld']; if($CF_FIELDS['nachricht']['pflicht']){ echo $CF_SETTINGS['pflichtfeld']; } ?></label>
    <?php
    echo $FORM->createField(
      'nachricht',
      '',
      'textarea',
      $CF_FIELDS['nachricht']['value'],
      '',
      '',
      ($CF_FIELDS['nachricht']['pflicht'] ? ' data-validation="required"' : '')
    );
    ?>
  </div>

<div class="cols cols-form check-mand">
   <div class="contactform-cb">
      <input type="checkbox" id="datenschutz" name="datenschutz" value="1" data-validation="required" >
      <label for="datenschutz">Die derzeit gültige Schulordnung, Gebührenordnung und die Datenschutzerklärung laut EU-DSGVO sind mir bekannt und werden von mir/uns akzeptiert. Die ausführlichen Informationen finden Sie <a href="<?php echo $staticlinks['downloads']['url'].'#c' ?>"> hier</a>.</label>
   </div>
  </div>

  <hr>

  <div class="buttons">
    <button name="kontaktform_send" type="submit" class="button-kontakt btn-arrow sprite-after">Anmeldung</button>
  </div>

</form>
<?php
}
?>
</div>
