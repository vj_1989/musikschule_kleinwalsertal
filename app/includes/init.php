<?php

session_start();

// Debuginfos on DEV1
$debug = false;
if (strpos($_SERVER['SERVER_NAME'], '-dev1')) {
    ini_set('display_errors', true);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL & ~E_NOTICE);
    $debug = true;
} else {
    error_reporting(0);
}
define('DEBUG', $debug);

$lang = 'de';
$root = '';

if (isset($_GET['lang']) && file_exists('includes/lang/' . $_GET['lang'] . '.php')) {
    $lang = filter_input(INPUT_GET, 'lang');
    $root = '/' . $lang;
    include 'includes/lang/' . $lang . '.php';
}
include 'includes/lang/de.php';

define('LANG', $lang);
define('ROOT', $root);

$contactform_sent = false;
$is_front = false;

define('KUNDEN_ID', 758);
define('API_PASSWORD', 'kBU7iMZundqPeSglcK6SHMkTbaszI');
define('API_TOKEN', 'f15223dcc0da90206acdce51c6a9e24938b18665165a819f1abb69233c068caekBU7iMZundqPeSglcK6SHMkTbaszI');

define('KUNDEN_BETRIEB', 'Musikschule Kleinwalsertal');
define('KUNDEN_NAME', '@ToDo Name');
define('KUNDEN_STRASSE', '	Engelbert-Kessler-Str. 34');
define('KUNDEN_ORT', 'A-6991 Riezlern');
define('KUNDEN_LAND', 'Österreich');
define('KUNDEN_TEL', 'Tel.:+43 664 8537910');
define('KUNDEN_FAX', '@ToDo Fax');
define('KUNDEN_EMAIL', 'info@musikschule-kleinwalsertal.at');

if (strpos($_SERVER['SERVER_NAME'], '-dev1')) {
    define('DOMAIN', 'http://musikschule-kleinwalsertal.werbewind-dev1.com');
    #define('IMG_PATH', '//http://www.musikschule-kleinwalsertal.at/images-ww/');
    define('IMG_PATH', 'https://server.werbewind.com/home/musikschule_kleinwalsertal/dist/images-ww/');
} else {
    define('DOMAIN', 'https://www.musikschule-kleinwalsertal.at');
    define('IMG_PATH', '/images-ww/');
}
define('LOGIN_AUTOLOAD_DIR', $include_dir . 'login/login2/src/');

// Register autoloader
function autoload_login($class) {
    $c = LOGIN_AUTOLOAD_DIR . str_replace('_', '/', $class) . '.php';
    if (file_exists($c)) {
        include_once $c;
    }
}

spl_autoload_register('autoload_login');

// Database connection
$DA102 = new Core_DataAccess($include_dir . 'login/login2/src/Modules/RedCms/dbconfig.php');

// CMS main class
$REDCMS_MODEL = new Modules_RedCms_Model($DA102);
$redcmssettings = $REDCMS_MODEL->getSettings(KUNDEN_ID);

// CMS navigation
$REDCMS_NAVI = new Modules_RedCms_Navi($DA102, $redcmssettings);

// CMS pages
$REDCMS_PAGES = new Modules_RedCms_Pages($DA102, $redcmssettings);

// CMS templates
$REDCMS_TPL = new Modules_RedCms_Templates(array(
    'image-captions' => true,
    'changer-class' => 'flexslider',
    'content-width' => 1440,
        ));
$REDCMS_TPL->setImgPath(IMG_PATH);

// Another autoloader for some older tools
// @todo check for removal
require_once $include_dir . 'tools/init.php';

/* if (!isset($_SESSION['VisitorSource'])) {
  $_SESSION['VisitorSource'] = '';
  if (!empty($_GET['landingpage'])) {
  $_SESSION['VisitorSource'] = trim(htmlspecialchars(strip_tags($_GET['landingpage'])));
  }
  else if (!empty($_GET['vs'])) {
  $_SESSION['VisitorSource'] = trim(htmlspecialchars(strip_tags($_GET['vs'])));
  }
  else if (!empty($_GET['utm_source']) || !empty($_GET['gclid'])) {
  $_SESSION['VisitorSource'] = 'ADWORDS';
  }
  } */

// Google Analytics, Facebook and stuff
#include $include_dir.'tools/Apis/client_api_data.php';

$redcmspages = $REDCMS_PAGES->getPublicPages(LANG);

// Store page IDs for linking from static links
$staticlinks = array(
    'impressum' => !empty($redcmspages[3982]) ? $redcmspages[3982] : null,
    'kontakt' => !empty($redcmspages[3981]) ? $redcmspages[3981] : null,
    'preise' => !empty($redcmspages[0]) ? $redcmspages[0] : null,
    'register' => !empty($redcmspages[4003]) ? $redcmspages[4003] : null,
    'events' => !empty($redcmspages[4223]) ? $redcmspages[4223] : null,
    'wajuba' => !empty($redcmspages[4005]) ? $redcmspages[4005] : null,
    'unterrichtsangebot' => !empty($redcmspages[4004]) ? $redcmspages[4004] : null,
    'aktuelles' => !empty($redcmspages[4007]) ? $redcmspages[4007] : null,
	'bildergallery' => !empty($redcmspages[4008]) ? $redcmspages[4008] : null,
	 'downloads'   => !empty($redcmspages[4011]) ? $redcmspages[4011] : null,
  'links'   => !empty($redcmspages[4010]) ? $redcmspages[4010] : null,
);

$parsed = parse_url($_SERVER['REQUEST_URI']);
$uri = $parsed['path'];
if (substr($uri, 0, 4) == '/en/') {
    $uri = substr($uri, 3);
}
if ($uri == '/') {
    // Check which page should be shown as front page
    $redcmsitem = $REDCMS_NAVI->getNaviItemByUrl($REDCMS_NAVI->getFrontUrl(), LANG);
    $is_front = true;
} else {
    $redcmsitem = $REDCMS_NAVI->getNaviItemByUrl($uri, LANG);
}
$rednews2_settings = array(
    'layout' => 1,
    'heading' => 2,
    'anchor' => 'c',
    'heading-article' => 2,
    'image-loading' => 'unveil', #'lazyload',
    'image-nojs' => false,
    'readmore' => 'Weiterlesen',
    'tags' => array(),
    'url_suffix' => '',
    'pic-wrapper' => true,
    'sort' => 'random'
);
$REDNEWS2_TPL = new Modules_RedNews2_Templates(KUNDEN_ID, $rednews2_settings, '../../../');
$REDNEWS2_TPL->setImgPath(IMG_PATH);
$rednews2_systemsettings = $REDNEWS2_TPL->getSystemSettings();



// No item found...
if (intval($redcmsitem['id']) == 0) {
    $page_exists = false;
    $REDCMS_REWRITER = new Modules_RedCms_Rewriter($DA102, $redcmssettings);
    $rewrite = $REDCMS_REWRITER->getRewriteByOldUrl($uri);
    if (!empty($rewrite)) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location:" . DOMAIN . $rewrite['url_neu']);
        exit();
    } else {
        if ($page_exists === false) {
            $parsed = parse_url($uri);
            $test = explode('/', $parsed['path']);
            $test1 = array_pop($test);
            $test2 = array_pop($test);

			
            $test_temp = explode('-', $test1);

            if ($test_temp[0] == 'event') {
				$eid = array_pop($test_temp);
				
                $params = array(
                    'results_per_page' => 15,
                    'in_house_only' => false
                );
				$WW_EVENTS = new Events_Main($params);
                $event = $WW_EVENTS->getEvent($eid, LANG);
				
                if (intval($event['event_id']) > 0) {
                    $page_exists = true;
                    $redcmsitem = $REDCMS_NAVI->getNaviItemByUrl($staticlinks['events']['url']);
                    $redcmsitem['seo_title'] = $event['titel'];
                    $redcmsitem['url'] .= '/' . Core_Strings::friendlyURL($event['titel']) . '-event-' . $event['event_id'];
                } else {
                    header("HTTP/1.1 301 Moved Permanently");
                    header("Location:" . DOMAIN . $staticlinks['events']['url']);
                    exit();
                }
                $uri = $staticlinks['events']['url'];
            } else {
                $rednews2_article_id = $REDNEWS2_TPL->getArticleIdByAlias($test1);
                if ($rednews2_article_id > 0) {
                    $page_exists = true;
                    $show_heading = false;

                    $public = true;
                    if (isset($_GET['preview'])) {
                        $public = false;
                    }
                    $REDNEWS2_TPL->updateSettings('heading-article', 2);
                    $REDNEWS2_TPL->updateSettings('show-subtitle', true);
                    $rednews2_article = $REDNEWS2_TPL->showArticle($rednews2_article_id, $public);

                    $redcmsitem = $REDCMS_NAVI->getNaviItemByUrl($staticlinks['aktuelles']['url']);
                    $redcmsitem['seo_title'] = $rednews2_article['seo_titel'];
                    $redcmsitem['seo_description'] = $rednews2_article['seo_description'];
                    $redcmsitem['url'] .= '/' . $rednews2_article['seo_urlalias'];
                    $uri = $staticlinks['aktuelles']['url'];
                }
            }
        }
        if ($page_exists === false) {
            header("HTTP/1.0 404 Not Found");
            $redcmsitem = $REDCMS_NAVI->getNaviItem($redcmssettings['page404']);
        }
    }
}

define('URI', $uri);

if ($redcmsitem['seo_title_global'] == 1) {
    $redcmsitem['seo_title'] .= $redcmssettings['seo_title'];
}
$redcmsitem['url'] = $redcmsitem['url'];

$redcmspage = $REDCMS_PAGES->getPage($redcmsitem['seite_id'], false, LANG);

// Get all navi items
$navifilter = array(
    'lang' => LANG
);
$items = $REDCMS_NAVI->getNaviItems($navifilter);

// Get navi HTML
$navioptions = array(
    'root' => ROOT,
    'current' => URI
);
$navi = $REDCMS_NAVI->buildNaviHtml($items, $navioptions);

// Array for storing CSS classes which will be added to <body>
$bodyClass = $REDCMS_NAVI->getCssClasses();

$bodyClass[] = 'page-' . $redcmspage['id'];

if (!empty($uri)) {
    $bodyClass[] = implode(' ', array_filter(explode('/', $uri)));
}
if ($is_front === true) {
    $bodyClass[] = 'home';
}
if (!empty($_COOKIE['accept_cookies'])) {
    $bodyClass[] = 'accept-cookies';
}
$bodyClass = array_filter($bodyClass);

// Function for translating dynamic weather data
// @todo check for replacement, maybe in a class Lib_Weather
function translateWeatherType($s) {
    $s = str_replace('bedeckt', LANG_WEATHER_BEDECKT, $s);
    $s = str_replace('Gewitter', LANG_WEATHER_GEWITTER, $s);
    $s = str_replace('leicht bewölkt', LANG_WEATHER_LEICHTBEWOELKT, $s);
    $s = str_replace('Nebel', LANG_WEATHER_NEBEL, $s);
    $s = str_replace('Regen', LANG_WEATHER_REGEN, $s);
    $s = str_replace('Schauer', LANG_WEATHER_SCHAUER, $s);
    $s = str_replace('Schneefall', LANG_WEATHER_SCHNEEFALL, $s);
    $s = str_replace('Schneeschauer', LANG_WEATHER_SCHNEESCHAUER, $s);
    $s = str_replace('Sonnig', LANG_WEATHER_SONNIG, $s);
    $s = str_replace('Sprühregen', LANG_WEATHER_SPRUEHREGEN, $s);
    $s = str_replace('wolkig', LANG_WEATHER_WOLKIG, $s);

    return $s;
}

?>
