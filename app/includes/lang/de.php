<?php
@define('LANG_ACTION_MAIL',    'Mail-Anfrage');
@define('LANG_ACTION_VOUCHER', 'Gutscheine');

@define('LANG_INQUIRE', 'anfragen');

@define('LANG_CONTACT',                'Kontakt');
@define('LANG_CONTACT_TEXT',						'Sie haben Fragen, Wünsche oder eine konkrete Verfügbarkeitsanfrage? Dann nutzen Sie unser Kontaktformular, wir melden uns umgehend zurück!');
@define('LANG_CONTACT_RRV',            'Wir empfehlen den Abschluss einer <a href="https://www.elviab2b.de/elvia/booking/eltravag.nsf/ElviaBookEntryTA?OpenForm&amp;Error=9001&amp;AN=HTL508&amp;PF=000000000000010&amp;PT=RRHOT" target="_blank" rel="noopener">Reiserücktrittsversicherung</a> (externer Anbieter).');
@define('LANG_CONTACT_PRIVACY',        'Ihre Daten werden sicher (SSL-verschlüsselt) übertragen. Bitte beachten Sie auch unseren <a href="{{impressum}}">Datenschutzhinweis</a>.');
@define('LANG_CONTACT_MORE_DATA',      'Damit wir Sie besser erreichen können, würden wir uns über weitere Kontaktdaten freuen. Die Daten werden weder weitergegeben noch für Werbezwecke verwendet.');
@define('LANG_CONTACT_YOURDATA',       'Ihre Daten');
@define('LANG_CONTACT_SALUTATION',     'Anrede');
@define('LANG_CONTACT_SALUTATION_MS',  'Frau');
@define('LANG_CONTACT_SALUTATION_MR',  'Herr');
@define('LANG_CONTACT_SALUTATION_F',   'Familie');
@define('LANG_CONTACT_SALUTATION_C',   'Firma');
@define('LANG_CONTACT_FIRSTNAME',      'Vorname');
@define('LANG_CONTACT_LASTNAME',       'Nachname');
@define('LANG_CONTACT_COMPANY',        'Firma');
@define('LANG_CONTACT_COUNTRY',        'Staat');
@define('LANG_CONTACT_STREET',         'Straße');
@define('LANG_CONTACT_HOUSENR',        'Hausnummer');
@define('LANG_CONTACT_POSTCODE',       'Postleitzahl');
@define('LANG_CONTACT_CITY',           'Ort');
@define('LANG_CONTACT_PHONE',          'Telefon');
@define('LANG_CONTACT_MOBILE',         'Handy');
@define('LANG_CONTACT_EMAIL',          'Email');
@define('LANG_CONTACT_YOURWISHES',     'Ihre Wünsche');
@define('LANG_CONTACT_ACCOMMODATION',  'Unterkunft');
@define('LANG_CONTACT_PERSONS',        'Personen');
@define('LANG_CONTACT_ARRIVAL',        'Anreise');
@define('LANG_CONTACT_DEPARTURE',      'Abreise');
@define('LANG_CONTACT_ACCOMMODATION',  'Unterkunft');
@define('LANG_CONTACT_ADULTS',         'Erwachsene');
@define('LANG_CONTACT_CHILDRENAGE',    'Alter Kind');
@define('LANG_CONTACT_NEWSLETTER',     'Die Hinweise zum <a href="'.ROOT.'impressum#datenschutz">Datenschutz</a> habe ich gelesen und zur Kenntnis genommen.<br><br>Ich bin damit einverstanden, dass die Werbewind GmbH meine angegebenen Daten zum Zwecke der Bearbeitung der Anfrage(n) speichern und weiterverarbeiten darf. Eine Übermittlung meiner Daten an Dritte findet nicht statt und ist auch nicht geplant.<br><br>Meine Einwilligung kann ich jederzeit bei der xy GmbH, Straße, Hausnr., D-xxxxx xxx, E-Mail info@xxx.de ohne Angabe von Gründen für die Zukunft widerrufen.');#@define('LANG_CONTACT_NEWSLETTER',     'Newsletter - Bei Anmeldung zum Newsletter wird Ihre Email Adresse für eigene Werbezwecke genutzt, bis Sie sich vom Newsletter abmelden. Die Abmeldung ist jederzeit möglich.');
@define('LANG_CONTACT_MESSAGE',        'Ihre Nachricht');
@define('LANG_CONTACT_SUBMIT',         'Senden');
@define('LANG_CONTACT_SUBMIT2',        'unverbindlich <strong>anfragen</strong>');
@define('LANG_CONTACT_OPTIONAL_DATA',  'freiwillige Daten eingeben');
@define('LANG_CONTACT_PERSONS',        'Personen');
@define('LANG_CONTACT_DAYS',           'Tage');
@define('LANG_CONTACT_MISC',           'Sonstiges');
@define('LANG_CONTACT_EXPRESSREQUEST', 'Expressanfrage');
@define('LANG_CONTACT_REFERRER',       'Bitte geben Sie uns hier kurz an, wie Sie auf unsere Seite aufmerksam wurden. Vielen Dank.');
@define('LANG_CONTACT_SUCCESS',        'Vielen Dank für Ihre Nachricht. Wir werden uns schnellstmöglich mit Ihnen in Verbindung setzen.');
@define('LANG_CONTACT_NL_SUCCESS',     'Vielen Dank für Ihre Anmeldung. Sie erhalten in Kürze eine Email, in der Sie einen Bestätigungslink klicken müssen, um die Anmeldung zu bestätigen. Sollte diese Email nicht innerhalb der nächsten Minuten bei Ihnen eintreffen, schauen Sie bitte auch in den Spam-Ordner Ihres Email-Postfachs.');
@define('LANG_CONTACT_ERROR',		       'Anfrage konnte nicht gesendet werden. Folgende Angaben sind nicht korrekt:');
@define('LANG_NEWSLETTER_ERROR',	     'Anmeldung konnte nicht durchgeführt werden. Folgende Angaben sind nicht korrekt:');
@define('LANG_CONTACT_MANDATORY', 	   'Pflichtfeld');

@define('LANG_FOOTER_CONTACT_PHONE', 'Tel');
@define('LANG_FOOTER_CONTACT_SEND',  'E-Mail Kontakt');

@define('LANG_FOOTER_IMPRINT', 'Impressum');
@define('LANG_FOOTER_PRIVACY', 'Datenschutz');
@define('LANG_FOOTER_LEGAL',   'Rechtliche Hinweise');

@define('LANG_BOX_WEATHER',       'Wetter');
@define('LANG_BOX_WEATHER_TODAY', 'Heute');
@define('LANG_BOX_WEATHER_MORE',  'Wettervorschau');

@define('LANG_BOX_FACEBOOK',        'Facebook');
@define('LANG_BOX_FACEBOOK_FAN',    'Werden Sie ein Fan unserer Facebook Seite!');
@define('LANG_BOX_FACEBOOK_BUTTON', 'zur Seite');

@define('LANG_WEATHER_HEADING',           'Aktuelles Wetter für');
@define('LANG_WEATHER_SUBHEADING',        'Wettervorhersage für die kommenden %s Tage.');
@define('LANG_WEATHER_ZUSTAND',           'Wetterzustand');
@define('LANG_WEATHER_TODAY',             'Heute');
@define('LANG_WEATHER_MORGENS',           'Morgens');
@define('LANG_WEATHER_MITTAGS',           'Mittags');
@define('LANG_WEATHER_ABENDS',            'Abends');
@define('LANG_WEATHER_TEMPERATUR',        'Temperatur');
@define('LANG_WEATHER_NIEDERSCHL',        'Niederschlagswahrscheinlichkeit ');
@define('LANG_WEATHER_WINDGESCHW',        'Windgeschwindigkeit');
@define('LANG_WEATHER_WINDRICHTUNG',      'Windrichtung');
@define('LANG_WEATHER_BEDECKT',           'bedeckt');
@define('LANG_WEATHER_LEICHTES_GEWITTER', 'leichtes Gewitter');
@define('LANG_WEATHER_GEWITTER',          'Gewitter');
@define('LANG_WEATHER_LEICHTBEWOELKT',    'leicht bewölkt');
@define('LANG_WEATHER_NEBEL',             'Nebel');
@define('LANG_WEATHER_LEICHTER_REGEN',    'leichter Regen');
@define('LANG_WEATHER_REGEN',             'Regen');
@define('LANG_WEATHER_SCHAUER',           'Schauer');
@define('LANG_WEATHER_SCHNEEFALL',        'Schneefall');
@define('LANG_WEATHER_SCHNEESCHAUER',     'Schneeschauer');
@define('LANG_WEATHER_SONNIG',            'Sonnig');
@define('LANG_WEATHER_SPRUEHREGEN',       'Sprühregen');
@define('LANG_WEATHER_WOLKIG',            'Wolkig');

@define('LANG_MONDAY',    'Montag');
@define('LANG_TUESDAY',   'Dienstag');
@define('LANG_WEDNESDAY', 'Mittwoch');
@define('LANG_THURSDAY',  'Donnerstag');
@define('LANG_FRIDAY',    'Freitag');
@define('LANG_SATURDAY',  'Samstag');
@define('LANG_SUNDAY',    'Sonntag');

@define('LANG_ROUTE_HEADING', 'Route mit Google-Maps berechnen');
@define('LANG_ROUTE_LEGEND',  'Ihr Standort');
@define('LANG_ROUTE_SUBMIT',  'Route berechnen');

@define('LANG_BOX_BERGBAHN',      'Sommer Bergbahnticket');
@define('LANG_BOX_BERGBAHN_TEXT', 'Exklusiv für unsere Gäste – Bergbahnticket inklusive von Mai bis November!');
@define('LANG_BOX_BERGBAHN_MORE', 'mehr Infos');
@define('LANG_BOOK',              'buchen');
@define('LANG_BOX_HOLIDAYCHECK',  'Holiday<br>Check');
?>