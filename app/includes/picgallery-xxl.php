<?php
$PGXXL = new PicGalleryXxl_Main();

$PGXXL->setNewImgPath(IMG_PATH.'galleryxl/');

$album = $PGXXL->getAlbum(252);
$pics  = $PGXXL->getPictures(252);

$data = array();

if (!empty($pics)) {

  foreach($pics as $pic) {

    $title = KUNDEN_BETRIEB;

    if (!empty($pic['titel'])) {
      $title .= ' - '.$pic['titel'];
    }

    $data[] = array(
      'thumb'   => str_replace('/xl/', '/m/', $pic['bild']),
      'src'     => $pic['bild'],
      'subHtml' => '<div class="customHtml"><h4>'.$title.'</h4><p>'.(!empty($pic['copyright']) ? '&copy; '.$pic['copyright'] : '').'</p></div>',
      'title'   => $title
    );

  }

  echo '<script>var picgalleryXxlPics='.json_encode($data).'</script>';

}
?>