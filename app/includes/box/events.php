<?php
if (!$WW_EVENTS instanceof Events_Main) {
  $params = array(
    'results_per_page' => 15,
    'in_house_only'    => false
  );
  $WW_EVENTS = new Events_Main($params);
}

$boxevents = $WW_EVENTS->getEvents();
$be        = array_slice($boxevents, 0, 4);

if (!empty($be)) {

  $content = '';

  $content .= '<div class="box box-events">';
  $content .= '<div class="box-heading">Veranstaltungen</div>';
  $content .= '<div class="box-content">';

  $day = '';

  foreach($be as $e){

    $href = Lib_Strings::makeURL($e['titel']).'-event-'.$e['event_id'];

    $content .= '<div class="clearfix box-item"';
    #$content .= ' itemprop="event" itemscope itemtype="http://schema.org/Event"';
    $content .= '>';
    $content .= '<div class="box-date">';
    #$content .= '<meta itemprop="startDate" content="'.$e['start'].'">';
    if($e['ende'] > $e['start']){
      $content .= 'Dauerveranstaltung';
      #$content .= '<meta itemprop="endDate" content="'.$e['ende'].'">';
    }
    else if($day != $e['start']) {
      $datetime = $e['start'];
      if(!empty($e['zeit'])) {
        $datetime .= 'T'.$e['zeit'];
      }
      $content .= '<time datetime="'.$datetime.'">'.Lib_Dates::getWeekdayName($e['wochentag']).', '.$e['start_f'].'</time>';
      $day = $e['start'];
    }
    $content .= '</div>'; // box-date
    if(!empty($e['zeit'])) {
      $content .= '<div class="box-time">'.$e['zeit'].' Uhr</div>';
    }
    $content .= '<div class="box-link"><a href="'.$href.'"';
    #$content .= ' itemprop="url"';
    $content .= '>';
    #$content .= '<span itemprop="name">';
    $content .= $e['titel'];
    #$content .= '</span>';
    $content .= '</a></div>';
    $content .= '</div>'; // box-item

  }
  $content .= '<div class="box-more"><a href="'.$staticlinks['veranstaltungen']['url'].'#c">alle Termine</a></div>';
  $content .= '</div>'; // box-content
  $content .= '</div>'; // box

  echo $content;
}
?>