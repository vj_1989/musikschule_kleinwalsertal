// Generated on 2018-12-20 using generator-werbewind-mf 0.1.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Configurable paths
	var config = {
		app: 'app',
		dist: 'dist'
	};

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Project settings
		config: config,

		// Watches files for changes and runs tasks based on the changed files
		watch: {
			bower: {
				files: ['bower.json'],
				tasks: ['bowerInstall']
			},
			js: {
				files: ['<%= config.app %>/scripts/{,*/}*.js'],
				tasks: ['jshint'],
				options: {
					livereload: true
				}
			},
			gruntfile: {
				files: ['Gruntfile.js']
			},
			styles: {
				files: ['<%= config.app %>/styles/{,*/}*.css'],
				tasks: ['newer:copy:styles', 'autoprefixer']
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'<%= config.app %>/{,*/}*.php',
					'.tmp/styles/{,*/}*.css',
					'<%= config.app %>/images/{,*/}*'
				]
			}
		},

		// The actual grunt server settings
		connect: {
			options: {
				port: 9000,
				open: true,
				livereload: 35729,
				// Change this to '0.0.0.0' to access the server from outside
				hostname: 'localhost'
			},
			livereload: {
				options: {
					middleware: function(connect) {
						return [
							connect.static('.tmp'),
							connect().use('/bower_components', connect.static('./bower_components')),
							connect.static(config.app)
						];
					}
				}
			},
			test: {
				options: {
					open: false,
					port: 9001,
					middleware: function(connect) {
						return [
							connect.static('.tmp'),
							connect.static('test'),
							connect().use('/bower_components', connect.static('./bower_components')),
							connect.static(config.app)
						];
					}
				}
			},
			dist: {
				options: {
					base: '<%= config.dist %>',
					livereload: false
				}
			}
		},

		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp/*',
						'.tmp',
						'<%= config.dist %>/*',
						'!<%= config.dist %>/.git*'
					]
				}]
			},
			server: '.tmp'
		},

		// Make sure code styles are up to par and there are no obvious mistakes
		jshint: {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish')
			},
			all: [
				'Gruntfile.js',
				'<%= config.app %>/scripts/{,*/}*.js',
				'!<%= config.app %>/scripts/vendor/*',
				'!<%= config.app %>/scripts/tools/*'
			]
		},

		// Add vendor prefixed styles and rem fallback
		postcss: {
			options: {
				processors: [
					require('pixrem')({rootValue: 10}), // add fallbacks for rem units
					require('autoprefixer')({browsers: '> 5%, iOS 7'}), // add vendor prefixes
				]
			},
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/styles/',
					src: '{,*/}*.css',
					dest: '.tmp/styles/'
				}]
			},
			app: {
				options: {
					processors: [
						require('autoprefixer')({browsers: '> 5%, iOS 7'}) // add vendor prefixes
					]
				},
				files: [{
					expand: true,
					cwd: '<%= config.app %>/less/',
					src: '{,*/}*.less',
					dest: '<%= config.app %>/less/'
				},
				{
					expand: true,
					cwd: '<%= config.app %>/styles/',
					src: '{,*/}*.css',
					dest: '<%= config.app %>/styles/'
				}]
			}
		},
		
		
		// Add vendor prefixed styles
		//autoprefixer: {
		//	options: {
		//		browsers: ['> 5%']
		//	},
		//	app: {
		//		files: [{
		//			expand: true,
		//			cwd: 'app/styles/',
		//			src: '{,*/}*.css',
		//			dest: 'app/styles/'
		//		}]
		//	}
		//},
		

		// Automatically inject Bower components into the HTML file
		bowerInstall: {
			app: {
				src: ['<%= config.app %>/index.php'],
				exclude: ['bower_components/bootstrap/dist/js/bootstrap.js']
			}
		},

		// Renames files for browser caching purposes
		rev: {
			dist: {
				files: {
					src: [
						'<%= config.dist %>/scripts/{,*/}*.js',
						'<%= config.dist %>/styles/{,*/}*.css',
						'<%= config.dist %>/images/{,*/,*/*/}*.*',
						'<%= config.dist %>/*.{ico,png}',
						'!<%= config.dist %>/styles/fonts/{,*/}*.*',
						'!<%= config.dist %>/fonts/{,*/}*.*'
					]
				}
			}
		},
		// Replace strings that we need for dev but crashs the live
		'string-replace': {
			dist: {
				files: {
					'.tmp/index.php': '<%= config.app %>/index.php',
				},
				options: {
					replacements: [{
						pattern: /\.\.\/\.\.\//ig,
						replacement: '../'
					}]
				}
			}
		},
		// Reads HTML for usemin blocks to enable smart builds that automatically
		// concat, minify and revision files. Creates configurations in memory so
		// additional tasks can operate on them
		useminPrepare: {
			options: {
				dest: '<%= config.dist %>',
				root: '<%= config.app %>'
			},
			html: '.tmp/index.php'
		},

		// Performs rewrites based on rev and the useminPrepare configuration
		usemin: {
			options: {
				assetsDirs: ['<%= config.dist %>', '<%= config.dist %>/images']
			},
			html: ['<%= config.dist %>/{,*/,*/*/}*.php'],
			css: ['<%= config.dist %>/styles/{,*/}*.css']
		},

		// The following *-min tasks produce minified files in the dist folder
		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.app %>/images',
					src: '{,*/,*/*/}*.{gif,jpeg,jpg,png}',//src: '{,*/,*/*/}*.{gif,jpeg,jpg,png}',
					dest: '<%= config.dist %>/images'
				}]
			}
		},

		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.app %>/images',
					src: '{,*/}*.svg',
					dest: '<%= config.dist %>/images'
				}]
			}
		},

		htmlmin: {
			dist: {
				options: {
					collapseBooleanAttributes: true,
					collapseWhitespace: true,
					removeAttributeQuotes: true,
					removeCommentsFromCDATA: true,
					removeEmptyAttributes: true,
					removeOptionalTags: true,
					removeRedundantAttributes: true,
					useShortDoctype: true
				},
				files: [{
					expand: true,
					cwd: '<%= config.dist %>',
					//src: '{,*/}*.php',
					src: 'index.php',
					dest: '<%= config.dist %>'
				}]
			}
		},

		// By default, your `index.php`'s <!-- Usemin block --> will take care of
		// minification. These next options are pre-configured if you do not wish
		// to use the Usemin blocks.
		// cssmin: {
		//	 dist: {
		//		 files: {
		//			 '<%= config.dist %>/styles/main.css': [
		//				 '.tmp/styles/{,*/}*.css',
		//				 '<%= config.app %>/styles/{,*/}*.css'
		//			 ]
		//		 }
		//	 }
		// },
		// uglify: {
		//	 dist: {
		//		 files: {
		//			 '<%= config.dist %>/scripts/scripts.js': [
		//				 '<%= config.dist %>/scripts/scripts.js'
		//			 ]
		//		 }
		//	 }
		// },
		// concat: {
		//	 dist: {}
		// },

		// Copies remaining files to places other tasks can use
		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= config.app %>',
					dest: '<%= config.dist %>',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'images/{,*/}*.webp',
						'{,*/,*/*/}*.php',
						'styles/fonts/{,*/}*.*',
						'images/{,*/,*/*/}*.*',
						'fonts/{,*/,*/*/}*.*'
					]
				}]
			},
			fonts: {
				files: [{
					expand: true,
					dot: true,
					cwd: 'bower_components/lightgallery/dist',
					dest: '<%= config.dist %>',
					src: [
						'fonts/*.*'
					]
				},
				{
					expand: true,
					dot: true,
					cwd: 'bower_components/flexslider',
					dest: '<%= config.dist %>/styles',
					src: [
						'fonts/*.*'
					]
				}]
			},
			styles: {
				expand: true,
				dot: true,
				cwd: '<%= config.app %>/styles',
				dest: '.tmp/styles/',
				src: '{,*/}*.css'
			}
		},

		// Generates a custom Modernizr build that includes only the tests you
		// reference in your app
		modernizr: {
			dist: {
                'customTests': [],
                'dest': '<%= config.dist %>/scripts/vendor/modernizr.js',
                'tests': [
                ],
                'options': [
                    'setClasses'
                ],
                'uglify': true
			}
		},

		// Run some tasks in parallel to speed up build process
		concurrent: {
			server: [
				'copy:styles'
			],
			test: [
				'copy:styles'
			],
			dist: [
				'copy:styles',
				'imagemin',
				'svgmin'
			]
		}
	});


	grunt.registerTask('serve', function (target) {
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

		grunt.task.run([
			'clean:server',
			'concurrent:server',
			'autoprefixer',
			'connect:livereload',
			'watch'
		]);
	});

	grunt.registerTask('server', function (target) {
		grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
		grunt.task.run([target ? ('serve:' + target) : 'serve']);
	});

	grunt.registerTask('build', [
		'clean:dist',
		'string-replace',
		'useminPrepare',
		'concurrent:dist',
		'postcss:dist',
		'concat',
		'cssmin',
		'uglify',
		'copy:dist',
		'copy:fonts',
		'modernizr',
		'rev',
		'usemin',
		'htmlmin'
	]);

	grunt.registerTask('default', [
		'newer:jshint',
		'build'
	]);
	
	grunt.registerTask('prefixapp', [
		'postcss:app'
	]);
};
