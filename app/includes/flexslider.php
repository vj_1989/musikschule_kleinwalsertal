<?php
$images = $imagesl = $imagess = array();
if (!empty($redcmsitem['headerbild'])) {
  $images = $imagess = $imagesl = array(
		IMG_PATH.'orig/'.$redcmsitem['headerbild']
  );
}
else {

  $gallery = 1219; // @todo Specify customer specific gallery ID

  if ($redcmsitem['headerchanger'] > 0) {
    $gallery = $redcmsitem['headerchanger'];
  }

  #$PG = new PicGalleryFlashEditable_Main();
	$PG = new Modules_PicGalleryFlashEditable_Model(new Core_DataAccess(LOGIN_AUTOLOAD_DIR.'Modules/PicGalleryFlashEditable/dbconfig.php')); 
	
	$PG->setImgPath(IMG_PATH);

  $temp_images = $PG->getPublicGalleryPics($gallery, true);
  $images = $imagesl = $imagesm = $imagess = $titles = $texts = array();

  if (count($temp_images) > 0) {
    foreach($temp_images as $image) {
      $images[]  = $image['bild'];
      $imagesl[] = $image['l_bild'];
      $imagesm[] = $image['m_bild'];
      $imagess[] = $image['s_bild'];
        $titles[]  = $image['titel'];
      $texts[]   = $image['inhalt'];
	  $linkurls[]   = $image['linkurl'];
	  $linktexts[]   = $image['linktext'];
    }
  }

  if (empty($temp_images)) {
    $images = $imagesl = $imagess = array(
      '/images/header/default.jpg'
    );
  }
}

echo '<div id="flexslider" class="flexslider flexslider-top">';
echo '<ul class="slides">';
for ( $i = 0; $i < count($images); $i ++ ) {
  echo '<li>';
  echo '<picture>';
  echo '<source media="(max-width: 480px)" srcset="'.$imagess[$i].'" class="flexible">';
  echo '<source media="(max-width: 768px)" srcset="'.$imagesm[$i].'" class="flexible">';
  echo '<source media="(max-width: 1024px)" srcset="'.$imagesl[$i].'" class="flexible">';
  echo '<source srcset="'.$images[$i].'" class="flexible">';
  echo '<img class="big" src="'.$images[$i].'" alt="';
  if (!empty($redcmspage['titel'])) {
    echo htmlspecialchars($redcmspage['titel']);
  }
  echo '">';
  echo '<img class="small" src="'.$imagess[$i].'" alt="';
  if (!empty($redcmspage['titel'])) {
    echo htmlspecialchars($redcmspage['titel']);
  }
  echo '">';
  
  echo '<div class="flexslider-extras">';
	if(!empty($titles[$i])){
	echo '<div class="flexslider-info">';
	echo '<div class="flexslider-heading caveat-font">'.$titles[$i].'</div>';
	echo '<div class="flexslider-text">'.$texts[$i].'</div>';
	echo '</div>';
	}if(!empty($linkurls[$i])){
	echo '<a href="'.$linkurls[$i].'" class="button sprite-after btn-arrow">'.$linktexts[$i].'</a>';
	}
	echo  '</div>';
  echo '</picture>';
  echo '</li>';
}
echo '</ul>';
echo '<span class="flexslider-pinsel"></span>';
echo '</div>';
?>